=====================
Index database schema
=====================

The index files produced by the file indexer are actually small database. There is provision for two different databases, `duckdb <https://duckdb.org/>`_ or `SQLite <https://www.sqlite.org/index.html>`_. However, at this time only duckdb is enabled.

The schema used by the index is outlined below. However, keep in mind that the  data type of the indexed column is variable depending on what is detected in the input data file.

It is not expected that the user directly interacts with the index database but the schema is documented just in case they need to do any debugging or need to implement anything in another language.

``indexed`` table
-----------------

The ``indexed`` table stores the actual index information on file offset (seek) positions for the rows containing the indexed values.

.. include:: ./data_dict/indexed.rst

``metadata`` table
------------------

The ``metadata`` table stores high level information about the index. There will only be a single row in the ``metadata`` table.

.. include:: ./data_dict/metadata.rst

``columns`` table
-----------------

The ``column`` table stores information on each column that have been profiled in the input data file. It is where the index reader will gather information on data types. There will be a row for each column of the indexed data file.

.. include:: ./data_dict/columns.rst

``column_convert`` table
------------------------

The ``column_convert`` table is implemented to allow for the storage of specific conversion values that may be encountered/generated when building the index. These can then be re-applied later when the index is being read. Currently, only date conversion strings are stored in here.

.. include:: ./data_dict/column_convert.rst
