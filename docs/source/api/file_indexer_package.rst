``file_indexer`` package
========================

``file_indexer.build`` module
-----------------------------

.. automodule:: file_indexer.build
   :members:
   :undoc-members:
   :show-inheritance:


``file_indexer.query`` module
-----------------------------

.. automodule:: file_indexer.query
   :members:
   :undoc-members:
   :show-inheritance:


``file_indexer.readers`` module
-------------------------------

.. automodule:: file_indexer.readers
   :members:
   :undoc-members:
   :show-inheritance:


``file_indexer.database`` module
--------------------------------

.. automodule:: file_indexer.database
   :members:
   :undoc-members:
   :show-inheritance:


``file_indexer.common`` module
------------------------------

.. automodule:: file_indexer.common
   :members:
   :undoc-members:
   :show-inheritance:

``file_indexer.errors`` module
------------------------------

.. automodule:: file_indexer.errors
   :members:
   :undoc-members:
   :show-inheritance:
