======================
Command-line endpoints
======================

Below is a list of all the command line endpoints installed with the file-indexer.

``fidx-build``
--------------

.. argparse::
   :module: file_indexer.build
   :func: _init_cmd_args
   :prog: fidx-build


``fidx-query``
--------------

.. argparse::
   :module: file_indexer.query
   :func: _init_cmd_args
   :prog: fidx-query
