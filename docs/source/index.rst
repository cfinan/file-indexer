.. file-indexer documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

file-indexer
============

The `file-indexer <https://gitlab.com/cfinan/file-indexer>`_ package allows the creation of indexes on files. It is designed to be used on specific columns of tabular data and works best if the file is sorted on the indexing column. The files can be plain text or bgzip compressed (block gzip compressed), this is different from regular gzip and allows random seeking into the file. See the `biopython documentation <https://biopython.org/docs/1.75/api/Bio.bgzf.html>`_ for more information.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: Example code

   examples

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   schema
   scripts
   api

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
