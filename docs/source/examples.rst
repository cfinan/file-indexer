Example code
============

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   examples/using_file_indexer
   examples/sorting_the_input
   examples/indexing_a_large_file
