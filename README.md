# Getting Started with file-indexer

__version__: `0.3.0a0`

The file-indexer package enabled the indexing of bgzipped compressed files or uncompressed files. It is designed to index flat tabular data and each index represents data in a single column and is a duckdb database that references the value and offset into the file.

This can be useful for when you do not want to load anything into a database but want fast querying of a file. It works best if the file is sorted on the column if interest as the indexes are a lot smaller.

If you need to index multiple columns, you can do this in separate index files. However, as things get more complicated, you might want to consider other solutions such as a proper database.

There is [online](https://cfinan.gitlab.io/file-indexer/) documentation for file-indexer.

## Installation instructions
At present, file-indexer is undergoing development and no packages exist yet on PyPi. Therefore it is recommended that you install in either of the two ways listed below.

### Installation using conda
I maintain a conda package in my personal conda channel. To install from this please run:

```
conda install -c cfin -c bioconda -c conda-forge file-indexer
```

There are currently builds for Python v3.9 and v3.10 for Linux-64 and Mac-osx. Please keep in mind that all development is carried out on Linux-64 and Python v3.9. I do not own a Mac so can't test on one, the conda build does run some import tests but that is it.

### Installation using pip
You can install using pip from the root of the cloned repository, first clone and cd into the repository root:

```
git clone git@gitlab.com:cfinan/file-indexer.git
cd file-indexer
```

Install the dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then install using pip
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the repository. The difference with this is that you can just to a `git pull` to update, or switch branches without re-installing:
```
python -m pip install -e .
```

### Conda dependencies
There are also conda yaml environment files in `./resources/conda/envs` that have the same contents as `requirements.txt` but for conda packages, so all the pre-requisites. I use this to install all the requirements via conda and then install the package as an editable pip install.

However, if you find these useful then please use them. There are Conda environments for Python v3.9, v3.10.

## Running tests
if you have installed via pip you can run the tests using:
```
pytest ./tests/
```

## Change log
To see what has been fixed and updated in the latest version, please see the [change log](./CHANGELOG.md).
