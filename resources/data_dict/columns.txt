column	name	data_type	description
1	column_idx	integer	The primary key for the ``columns`` table. Will correspond to a 1-based column order in the input data file.
2	name	varchar	The name of the column in the input file header. Length 100.
3	dtype	string	The data type of the data in the column. Can be ``varchar``, ``text``, ``int``, ``big_int``, ``small_int``, ``float``, ``date``, ``datetime``, ``bool``.
4	max_length	integer	The maximum length of the data in the column (as characters before any type conversion).
5	rows	big_integer	The total number of rows for the column. This should be the same for all of the columns.
6	not_null_rows	big_integer	The number of rows that contain defined values.
7	is_ascending	boolean	Is the column data arranged in ascending order.
8	is_descending	boolean	Is the column data arranged in decending order.
9	has_null	boolean	Does the column concain any NULL values.
10	is_index	boolean	Is the column the indix column. There should only be one column with a True value in this field.
