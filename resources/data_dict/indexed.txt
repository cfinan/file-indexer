column	name	data_type	description
1	row_idx	integer	The primary key. This is also the row number of the invex value in the input file.
2	idx_value	any	The indexed value. The data type of this column will change depending on the data type of the indexed column in the data file.
3	seek	big_integer	The seek position into the file.
4	seek_end	big_integer	The seek position at the end of the line run.
5	line_run	integer	The number of consecutive rows including the first row that the ``idx_value`` is repeated.
