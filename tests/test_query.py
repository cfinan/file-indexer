"""Some tests for the ``file_indexer.query`` module.
"""
from file_indexer import query
from sqlalchemy_config.orm_code import FileColumn
import pytest
import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "input_strs, exp_strs",
    (
        (['=aaa'], [('=', 'aaa')]),
        (['==aaa'], [('==', 'aaa')]),
        (['=~^Z'], [('=~', '^Z')]),
        (['i=~^Z'], [('=~', '^Z', 'i')]),
        (['<10'], [('<', '10')]),
        (['<= 10'], [('<=', '10')]),
        (['between 0.234, 10'], [('between', '0.234', '10')]),
        (['=    2000-20-01'], [('=', '2000-20-01')]),
        (['WHERE mycol == "hello"'], [('WHERE', 'mycol == "hello"')]),
        (
            ['WHERE (mycol == "hello") OR (mycol == "world")'],
            [('WHERE', '(mycol == "hello") OR (mycol == "world")')]
        ),
    )
)
def test_process_query_terms(input_strs, exp_strs):
    res_strs = query._process_query_terms(input_strs)
    # pp.pprint(res_strs)
    # pp.pprint(exp_strs)
    assert res_strs == exp_strs, "wrong output"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "input_strs, exp_strs, dtype",
    (
        ([('=', 'aaa')], [('=', 'aaa')], FileColumn.Dtype.VARCHAR),
        ([('==', 'aaa')], [('==', 'aaa')], FileColumn.Dtype.VARCHAR),
        ([('=~', '^Z')], [('=~', '^Z')], FileColumn.Dtype.VARCHAR),
        ([('=~', '^Z', 'i')], [('=~', '^Z', 'i')], FileColumn.Dtype.VARCHAR),
        ([('<', '10')], [('<', 10)], FileColumn.Dtype.INTEGER),
        ([('<=', '0.2345')], [('<=', 0.2345)], FileColumn.Dtype.FLOAT),
        ([('between', '0.234', '10')], [('between', 0.234, 10.0)],
         FileColumn.Dtype.FLOAT),
        # (['=    2000-20-01'], [('=', '2000-20-01')]),
        # (['WHERE mycol == "hello"'], [('WHERE', 'mycol == "hello"')]),
        # (
        #     ['WHERE (mycol == "hello") OR (mycol == "world")'],
        #     [('WHERE', '(mycol == "hello") OR (mycol == "world")')]
        # ),
    )
)
def test_cast_query_terms(input_strs, exp_strs, dtype):
    res_strs = query._cast_query_terms(input_strs, dtype)
    pp.pprint(res_strs)
    pp.pprint(exp_strs)
    # assert res_strs == exp_strs, "wrong output"
