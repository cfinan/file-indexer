"""Here are the actual test for the module: run with ``pytest test_module.py``.
There is no need to import conftest it happens automatically.
"""
from file_indexer import readers, build
from pyaddons import utils
from sqlalchemy_config import type_cast as tc
import pytest
import gzip
import os
import re
import common as test_common
# import pprint as pp

DELIMITER = "\t"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _extract_query(data, column, query):
    """Query the data to extract the comparison rows for the test.

    Parameters
    ----------
    data : `list` of `list` of `str`
        The input data. The first row is the header
    column : `str`
        The column the query will be applied against.
    query : `list` of `tuple`
        The query to extract from the data, this is mimicking an index query.

    Returns
    -------
    expected_data : `list` of `list` of `str`
        The expected results data.
    """
    expected_data = []
    for i in query:
        try:
            op, value = i
            flags = None
        except ValueError:
            op, value, flags = i

        if op in ['==', '=']:
            expected_data.extend(_get_equals(data, column, value))
        elif op == '=~':
            expected_data.extend(_get_regexp(data, column, value, flags))
        elif op == '<':
            expected_data.extend(_get_less_than(data, column, value))
        elif op == '>':
            expected_data.extend(_get_greater_than(data, column, value))
        elif op == '<=':
            expected_data.extend(_get_less_than_equal(data, column, value))
        elif op == '>=':
            expected_data.extend(_get_greater_than_equal(data, column, value))
        elif op == 'between':
            expected_data.extend(_get_between(data, column, value, flags))
        else:
            raise ValueError(f"unknown operator: {op}")
    expected_data.sort(key=lambda x: x[0])
    return expected_data


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_equals(data, column, value):
    """Get all the ``data`` rows when the ``column`` values equal ``value``.

    Parameters
    ----------
    data : `list` of `list` of `str`
        The input data. The first row is the header
    column : `str`
        The column the query will be applied against.
    value : `str`
        The value to test for equality.

    Returns
    -------
    matching_data : `list` of `list` of `str`
        The ``data`` rows where ``column`` values exactly equal ``value``.
    """
    idx = data[0].index(column)

    if isinstance(value, bool):
        return [i for i in data[1:] if tc.parse_bool_none(i[idx]) is value]
        # return_data = []
        # for i in data[1:]:
        #     i[idx] = tc.parse_bool_none(i[idx])
        #     if i[idx] is value:
        #         return_data.append(i)
        # return return_data
    return [i for i in data[1:] if i[idx] == value]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_regexp(data, column, value, flags):
    """Get all the ``data`` rows when the ``column`` values match the regular
    expression value ``value``.

    Parameters
    ----------
    data : `list` of `list` of `str`
        The input data. The first row is the header
    column : `str`
        The column the query will be applied against.
    value : `str`
        The regulat expression value to test for matching.

    Returns
    -------
    matching_data : `list` of `list` of `str`
        The ``data`` rows where ``column`` values match the regular
        expression value ``value``..
    """
    reg = re.compile(value)
    if flags is not None:
        reg = re.compile(value, re.IGNORECASE)

    idx = data[0].index(column)
    return [i for i in data[1:] if reg.search(i[idx])]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_less_than(data, column, value):
    """Get all the ``data`` rows when the ``column`` values equal ``value``.

    Parameters
    ----------
    data : `list` of `list` of `str`
        The input data. The first row is the header
    column : `str`
        The column the query will be applied against.
    value : `str`
        The value to test for equality.

    Returns
    -------
    matching_data : `list` of `list` of `str`
        The ``data`` rows where ``column`` values exactly equal ``value``.
    """
    idx = data[0].index(column)
    return [i for i in data[1:] if i[idx] != '' and float(i[idx]) < value]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_greater_than(data, column, value):
    """Get all the ``data`` rows when the ``column`` values equal ``value``.

    Parameters
    ----------
    data : `list` of `list` of `str`
        The input data. The first row is the header
    column : `str`
        The column the query will be applied against.
    value : `str`
        The value to test for equality.

    Returns
    -------
    matching_data : `list` of `list` of `str`
        The ``data`` rows where ``column`` values exactly equal ``value``.
    """
    idx = data[0].index(column)
    return [i for i in data[1:] if i[idx] != '' and float(i[idx]) > value]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_less_than_equal(data, column, value):
    """Get all the ``data`` rows when the ``column`` values equal ``value``.

    Parameters
    ----------
    data : `list` of `list` of `str`
        The input data. The first row is the header
    column : `str`
        The column the query will be applied against.
    value : `str`
        The value to test for equality.

    Returns
    -------
    matching_data : `list` of `list` of `str`
        The ``data`` rows where ``column`` values exactly equal ``value``.
    """
    idx = data[0].index(column)
    return [i for i in data[1:] if i[idx] != '' and float(i[idx]) <= value]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_greater_than_equal(data, column, value):
    """Get all the ``data`` rows when the ``column`` values equal ``value``.

    Parameters
    ----------
    data : `list` of `list` of `str`
        The input data. The first row is the header
    column : `str`
        The column the query will be applied against.
    value : `str`
        The value to test for equality.

    Returns
    -------
    matching_data : `list` of `list` of `str`
        The ``data`` rows where ``column`` values exactly equal ``value``.
    """
    idx = data[0].index(column)
    return [i for i in data[1:] if i[idx] != '' and float(i[idx]) >= value]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_between(data, column, lower, upper):
    """Get all the ``data`` rows when the ``column`` values equal ``value``.

    Parameters
    ----------
    data : `list` of `list` of `str`
        The input data. The first row is the header
    column : `str`
        The column the query will be applied against.
    value : `str`
        The value to test for equality.

    Returns
    -------
    matching_data : `list` of `list` of `str`
        The ``data`` rows where ``column`` values exactly equal ``value``.
    """
    idx = data[0].index(column)
    return [
        i for i in data[1:]
        if i[idx] != '' and float(i[idx]) >= lower and float(i[idx]) <= upper
    ]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "query, column, index_order",
    (
        ([('=', 'aaa')], 'repeated_text', 'file'),
        ([('=', 'aaa'), ('=~', '^z', 'i')], 'repeated_text', 'file'),
        ([('=', 'aaa')], 'unique_text', 'file'),
        ([('=~', '^z', 'i')], 'unique_text', 'file'),
        ([('<', 10)], 'row_number', 'file'),
        ([('<=', 10), ('>=', 50)], 'row_number', 'file'),
        ([('between', 10, 40)], 'row_number', 'file'),
        ([('between', 70, 90)], 'row_number', 'file'),
        ([('<', 1)], 'floats', 'file'),
        ([('between', 32, 33)], 'floats', 'file'),
        ([('=', True)], 'bool_string', 'file'),
        ([('=', False)], 'bool_string', 'file'),
        ([('=', True)], 'bool_string', 'file'),
        ([('=', False)], 'bool_string', 'file'),
        ([('=', True)], 'bool_integer', 'file'),
        ([('=', False)], 'bool_integer', 'file'),
        ([('=', '2000-20-01')], 'dates', 'file'),
        ([('=', 'aaa')], 'repeated_text', 'value'),
        ([('=', 'aaa'), ('=~', '^z', 'i')], 'repeated_text', 'value'),
        ([('=', 'aaa')], 'unique_text', 'value'),
        ([('=~', '^z', 'i')], 'unique_text', 'value'),
        ([('<', 10)], 'row_number', 'value'),
        ([('<=', 10), ('>=', 50)], 'row_number', 'value'),
        ([('between', 10, 40)], 'row_number', 'value'),
        ([('between', 70, 90)], 'row_number', 'value'),
        ([('<', 1)], 'floats', 'value'),
        ([('between', 32, 33)], 'floats', 'value'),
        ([('=', True)], 'bool_string', 'value'),
        ([('=', False)], 'bool_string', 'value'),
        ([('=', True)], 'bool_string', 'value'),
        ([('=', False)], 'bool_string', 'value'),
        ([('=', True)], 'bool_integer', 'value'),
        ([('=', False)], 'bool_integer', 'value'),
        ([('=', '2000-20-01')], 'dates', 'value'),
        ([('=~', 'A')], 'unicode', 'value'),
    )
)
def test_index_reader_bgzip(small_file, query, column, index_order):
    """Test that the expected rows are returned using the basic index reader.
    This does not test any data type conversion just expected row data. It
    does test different query types through.
    """
    indata = test_common._read_data(small_file)
    expected_data = _extract_query(indata, column, query)
    # pp.pprint(expected_data)

    data_file, index = build.build_index(
        small_file, column, delimiter=DELIMITER,
        tmpdir=os.path.dirname(small_file),
        index_order=index_order
    )

    with readers.IndexReader(data_file) as idx:
        nrows = idx.total_count(query)
        assert nrows == len(expected_data), "wrong row count"

        rows = [i for i in idx.query(query)]
        assert sorted(rows, key=lambda x: x[0]) == expected_data, "wrong rows"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "query, column, index_order",
    (
        ([('=', 'aaa')], 'repeated_text', 'file'),
        ([('=', 'aaa'), ('=~', '^z', 'i')], 'repeated_text', 'file'),
        ([('=', 'aaa')], 'unique_text', 'file'),
        ([('=~', '^z', 'i')], 'unique_text', 'file'),
        ([('<', 10)], 'row_number', 'file'),
        ([('<=', 10), ('>=', 50)], 'row_number', 'file'),
        ([('between', 10, 40)], 'row_number', 'file'),
        ([('between', 70, 90)], 'row_number', 'file'),
        ([('<', 1)], 'floats', 'file'),
        ([('between', 32, 33)], 'floats', 'file'),
        ([('=', True)], 'bool_string', 'file'),
        ([('=', False)], 'bool_string', 'file'),
        ([('=', True)], 'bool_string', 'file'),
        ([('=', False)], 'bool_string', 'file'),
        ([('=', True)], 'bool_integer', 'file'),
        ([('=', False)], 'bool_integer', 'file'),
        ([('=', '2000-20-01')], 'dates', 'file'),
        ([('=', 'aaa')], 'repeated_text', 'value'),
        ([('=', 'aaa'), ('=~', '^z', 'i')], 'repeated_text', 'value'),
        ([('=', 'aaa')], 'unique_text', 'value'),
        ([('=~', '^z', 'i')], 'unique_text', 'value'),
        ([('<', 10)], 'row_number', 'value'),
        ([('<=', 10), ('>=', 50)], 'row_number', 'value'),
        ([('between', 10, 40)], 'row_number', 'value'),
        ([('between', 70, 90)], 'row_number', 'value'),
        ([('<', 1)], 'floats', 'value'),
        ([('between', 32, 33)], 'floats', 'value'),
        ([('=', True)], 'bool_string', 'value'),
        ([('=', False)], 'bool_string', 'value'),
        ([('=', True)], 'bool_string', 'value'),
        ([('=', False)], 'bool_string', 'value'),
        ([('=', True)], 'bool_integer', 'value'),
        ([('=', False)], 'bool_integer', 'value'),
        ([('=', '2000-20-01')], 'dates', 'value'),
        ([('=~', 'A')], 'unicode', 'value'),
    )
)
def test_index_reader_text(tmpdir, small_file, query, column, index_order):
    """Test that the expected rows are returned using the basic index reader.
    This does not test any data type conversion just expected row data. If does
    test different query types through.
    """
    infile = test_common.write_data(small_file, tmpdir)
    indata = test_common._read_data(infile)
    # pp.pprint(indata)
    expected_data = _extract_query(indata, column, query)
    # pp.pprint(expected_data)

    data_file, index = build.build_index(
        infile, column, delimiter=DELIMITER,
        tmpdir=os.path.dirname(small_file),
        index_order=index_order
    )

    with readers.IndexReader(data_file) as idx:
        nrows = idx.total_count(query)
        assert len(expected_data) == nrows, "wrong row count"

        rows = [i for i in idx.query(query)]
        assert sorted(rows, key=lambda x: x[0]) == expected_data, "wrong rows"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "query, column, index_order",
    (
        ([('=', 'aaa')], 'repeated_text', 'file'),
        ([('=', 'aaa'), ('=~', '^z', 'i')], 'repeated_text', 'file'),
        ([('=', 'aaa')], 'unique_text', 'file'),
        ([('=~', '^z', 'i')], 'unique_text', 'file'),
        ([('<', 10)], 'row_number', 'file'),
        ([('<=', 10), ('>=', 50)], 'row_number', 'file'),
        ([('between', 10, 40)], 'row_number', 'file'),
        ([('between', 70, 90)], 'row_number', 'file'),
        ([('<', 1)], 'floats', 'file'),
        ([('between', 32, 33)], 'floats', 'file'),
        ([('=', True)], 'bool_string', 'file'),
        ([('=', False)], 'bool_string', 'file'),
        ([('=', True)], 'bool_string', 'file'),
        ([('=', False)], 'bool_string', 'file'),
        ([('=', True)], 'bool_integer', 'file'),
        ([('=', False)], 'bool_integer', 'file'),
        ([('=', '2000-20-01')], 'dates', 'file'),
        ([('=', 'aaa')], 'repeated_text', 'value'),
        ([('=', 'aaa'), ('=~', '^z', 'i')], 'repeated_text', 'value'),
        ([('=', 'aaa')], 'unique_text', 'value'),
        ([('=~', '^z', 'i')], 'unique_text', 'value'),
        ([('<', 10)], 'row_number', 'value'),
        ([('<=', 10), ('>=', 50)], 'row_number', 'value'),
        ([('between', 10, 40)], 'row_number', 'value'),
        ([('between', 70, 90)], 'row_number', 'value'),
        ([('<', 1)], 'floats', 'value'),
        ([('between', 32, 33)], 'floats', 'value'),
        ([('=', True)], 'bool_string', 'value'),
        ([('=', False)], 'bool_string', 'value'),
        ([('=', True)], 'bool_string', 'value'),
        ([('=', False)], 'bool_string', 'value'),
        ([('=', True)], 'bool_integer', 'value'),
        ([('=', False)], 'bool_integer', 'value'),
        ([('=', '2000-20-01')], 'dates', 'value'),
        ([('=~', 'A')], 'unicode', 'value'),
    )
)
def test_index_reader_outfile(tmpdir, small_file, query, column, index_order):
    """Test that the expected rows are returned using the basic index reader.
    This does not test any data type conversion just expected row data. If does
    test different query types through.
    """
    infile = test_common.write_data(small_file, tmpdir, method=gzip.open)
    indata = test_common._read_data(infile)
    expected_data = _extract_query(indata, column, query)
    # # pp.pprint(expected_data)
    out_idx = utils.get_temp_file(dir=tmpdir, suffix='.txt.gz')

    data_file, index = build.build_index(
        infile, column, outfile=out_idx,
        delimiter=DELIMITER,
        tmpdir=os.path.dirname(small_file),
        index_order=index_order,
        overwrite=True
    )
    assert out_idx == str(data_file), "wrong file"
    with readers.IndexReader(data_file) as idx:
        nrows = idx.total_count(query)
        assert len(expected_data) == nrows, "wrong row count"

        rows = [i for i in idx.query(query)]
        assert sorted(rows, key=lambda x: x[0]) == expected_data, "wrong rows"
