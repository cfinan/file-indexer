"""Test for some of the classes/functions in the ``file_indexer.common``
module
"""
from file_indexer import common
from pyaddons import utils
import common as test_common
import csv
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_bgzip_reader(small_file):
    """Test that the ``common.BgzfEncodingReader`` reads in tab delimited UTF8
    encoded data correctly (when compared to gzip)
    """
    exp_data = test_common._read_data(small_file)
    with common.BgzfEncodingReader(small_file) as fh:
        reader = csv.reader(fh, delimiter=test_common.DELIMITER)
        test_data = []
        for row in reader:
            test_data.append(row)

    for i, j in zip(exp_data, test_data):
        assert i == j, "rows are different"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_bgzip_writer(tmpdir, small_file):
    """Test that the ``common.BgzfEncodingWrites`` reads in UTF8 encoded data
    correctly.
    """
    exp_data = test_common._read_data(small_file)
    tmpfile = utils.get_temp_file(dir=tmpdir)

    with common.BgzfEncodingWriter(tmpfile) as outfh:
        writer = csv.writer(outfh, delimiter=test_common.DELIMITER)
        for row in exp_data:
            writer.writerow(row)

    test_data = test_common._read_data(tmpfile)

    for i, j in zip(exp_data, test_data):
        assert i == j, "rows are different"
