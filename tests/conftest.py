"""Fixtures (re-usable data components and settings) for tests
"""
from file_indexer.example_data import examples as ex
from file_indexer import readers
from pyaddons import utils
import shutil
import pytest
import os


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def small_file(tmpdir):
    """Illustrating returning a tmpdir (another builtin pytest fixture) from
    pytest (this is magically destroyed) after the test.
    """
    working_file = shutil.copy2(ex.get_data("small_file_path"), tmpdir)
    yield working_file
    os.unlink(working_file)



# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# @pytest.fixture()
# def yield_fixture(tmpdir):
#     """Values can also be yielded from a fixture. Allowing you to do stuff
#     after
#     """
#     yield tmpdir

#     # Do something else here after the test has run


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# @pytest.fixture(scope="session", autouse=True)
# def session_scope():
#     """Unlike the two fixtures above that are loaded each time they are called
#     from a test. The session scope fixtures are loaded once when the test
#     module is run.
#     """
#     return True


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# @pytest.fixture()
# def resource_available(tmpdir):
#     """This mimics a fixture that will supply a path to an external resource.
#     If the resource is available the fixture will return it, if not then it
#     will return False. The calling test can then ``assert resource_available``
#     and fail appropriately.
#     """
#     return tmpdir


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# @pytest.fixture()
# def resource_not_available():
#     """This mimics a fixture that will supply a path to an external resource.
#     If the resource is available the fixture will return it, if not then it
#     will return False. The calling test can then ``assert resource_available``
#     and fail appropriately.
#     """
#     return False
