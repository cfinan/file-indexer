import pytest
import importlib


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "import_loc",
    (
        'file_indexer.build',
        'file_indexer.common',
        'file_indexer.database',
        'file_indexer.errors',
        'file_indexer.query',
        'file_indexer.readers',
        'file_indexer.example_data.examples',
    )
)
def test_package_import(import_loc):
    """Test that the modules can be imported.
    """
    importlib.import_module(import_loc, package=None)
