from pyaddons import utils
import csv

DELIMITER = "\t"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _read_data(infile):
    """Read the whole of the infile into memory.

    Parameters
    ----------
    infile : `str`
        The path to the input file.

    Returns
    -------
    data : `list` of `list` of `str`
        The input data.
    """
    open_method = utils.get_open_method(infile)

    with open_method(infile, 'rt') as infile:
        reader = csv.reader(infile, delimiter=DELIMITER)
        return [row for row in reader]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def write_data(infile, outdir, method=open):
    """Read the whole of the infile into memory and write out as text.

    Parameters
    ----------
    infile : `str`
        The path to the input file.
    outfile : `str`
        The path to the input file.

    Returns
    -------
    data : `list` of `list` of `str`
        The input data.
    """
    open_method = utils.get_open_method(infile)
    tmpfile = utils.get_temp_file(dir=outdir)

    with method(tmpfile, 'wt') as outcsv:
        writer = csv.writer(outcsv, delimiter=DELIMITER)
        with open_method(infile, 'rt') as infile:
            reader = csv.reader(infile, delimiter=DELIMITER)
            for row in reader:
                writer.writerow(row)
    return tmpfile
