# Change log

## version `0.3.0a0`
### Fixed
* API - Fixed case sensitivity in index searches.

### Added
* Added a seek end column to the indexes. This may help in future for cross index queries.

### Updated
* Pandas>=v2 and duckdb >= 1 in requirements and conda builds
* Updated example notebooks to reflect some issues with duckdb and searching dates.

## version `0.2.2a0`
### Fixed
* API - Updated duckdb commands to reflect changes in v.1.0.0 defaults that were causing removal of first index data row.

## version `0.2.1a0`
### Fixed
* SCRIPT - Fixed file order bug by adding default cmd-line option for file order

## version `0.2.0a0`
* First complete build.
