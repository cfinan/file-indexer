__version__ = '0.3.0a0'

# Import examples into the main package
from .example_data import (
    examples
)
