"""Functions and classes for handing index database interaction.
"""
from file_indexer import __version__
from tqdm import tqdm
from pyaddons import utils
import sqlite3
import duckdb
import gzip
import csv
import os
import shutil
import re
import pprint as pp


ROW_IDX_COL_NAME = 'row_idx'
"""The column name for the row number in the index file and DB (`str`)
"""
IDX_VAL_COL_NAME = 'idx_value'
"""The column name for the index value in the index file and DB (`str`)
"""
SEEK_COL_NAME = 'seek'
"""The column name for the seek position value in the index file and DB (`str`)
"""
SEEK_END_COL_NAME = 'seek_end'
"""The column name for the end seek position value in the index file and DB
(`str`)
"""
LINE_RUN_COL_NAME = 'line_run'
"""The column name for the line run value in the index file and DB (`str`)
"""
INDEX_HEADER = [
    ROW_IDX_COL_NAME, IDX_VAL_COL_NAME, SEEK_COL_NAME,
    SEEK_END_COL_NAME, LINE_RUN_COL_NAME
]
"""The header row of an temp index file and the index table in the index
(`list` of `str`)
"""
ROW_IDX_COL_IDX = INDEX_HEADER.index(ROW_IDX_COL_NAME)
"""The column index for the row number column in the index file and DB
(`int`)
"""
IDX_VAL_COL_IDX = INDEX_HEADER.index(IDX_VAL_COL_NAME)
"""The column index for the index value in the index file and DB (`int`)
"""
SEEK_COL_IDX = INDEX_HEADER.index(SEEK_COL_NAME)
"""The column index for the seek position value in the index file and DB
(`int`)
"""
SEEK_END_COL_IDX = INDEX_HEADER.index(SEEK_END_COL_NAME)
"""The column index for the seek end position value in the index file and DB
(`int`)
"""
LINE_RUN_COL_IDX = INDEX_HEADER.index(LINE_RUN_COL_NAME)
"""The column index for the line run value in the index file and DB (`int`)
"""
_INDEX_DELIMITER = "\t"
"""The delimiter of the temp index file (`str`)
"""
_MISSING = ['']


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _BaseIndex(object):
    """Index database base class, do not use directly.

    Parameters
    ----------
    index_path : `str`
        The path to the index database file.
    tmpdir : `str`, optional, default: `NoneType`
        The temp directory to use, if not supplied will default to the system
        temp location.
    read_only : `bool`, optional, default: `True`
        Open the index in read only mode, this is needed for concurrent access.
    """
    COL_COLUMN_IDX = "column_idx"
    """The name of the primary column in the columns table (`str`)
    """
    COL_NAME = "name"
    """The name of the data_type in the columns table (`str`)
    """
    COL_DATA_TYPE = "data_type"
    """The name of the data_type in the columns table (`str`)
    """
    COL_MAX_LEN = "max_length"
    """The name of the max_length in the columns table (`str`)
    """
    COL_ROWS = "rows"
    """The name of the number of rows in the columns table (`str`)
    """
    COL_NOT_NULL_ROWS = "not_null_rows"
    """The name of the number of not null rows in the columns table
    (`str`)
    """
    COL_IS_ASC = "is_ascending"
    """The name of the is_ascending in the columns table (`str`)
    """
    COL_IS_DESC = "is_descending"
    """The name of the is_descending in the columns table (`str`)
    """
    COL_HAS_NULL = "has_null"
    """The name of the has_null in the columns table (`str`)
    """
    COL_IS_INDEX = "is_index"
    """The name of the is_index in the columns table (`str`)
    """
    COLUMN_TABLE_COLUMNS = [
        COL_COLUMN_IDX,
        COL_NAME,
        COL_DATA_TYPE,
        COL_MAX_LEN,
        COL_ROWS,
        COL_NOT_NULL_ROWS,
        COL_IS_ASC,
        COL_IS_DESC,
        COL_HAS_NULL,
        COL_IS_INDEX
    ]
    """The column order in the columns table (`list` or `str`)
    """
    CREATE_COLUMNS_TABLE = f"""
    CREATE TABLE columns (
    {COL_COLUMN_IDX} INTEGER PRIMARY KEY,
    {COL_NAME} VARCHAR(100) NOT NULL,
    {COL_DATA_TYPE} VARCHAR(100) NOT NULL,
    {COL_MAX_LEN} INTEGER NOT NULL,
    {COL_ROWS} BIGINT NOT NULL,
    {COL_NOT_NULL_ROWS} BIGINT NOT NULL,
    {COL_IS_ASC} BOOLEAN NOT NULL,
    {COL_IS_DESC} BOOLEAN NOT NULL,
    {COL_HAS_NULL} BOOLEAN NOT NULL,
    {COL_IS_INDEX} BOOLEAN NOT NULL
    )
    """
    """Create table statement for the indexed file column definitions (`str`)
    """

    COLCON_IDX = "column_convert_idx"
    """The name of the primary key column in the column_convert table (`str`)
    """
    COLCON_STR = "convert_str"
    """The name of the convert_str column in the column_convert table (`str`)
    """
    COLCON_VALUES = "convert_values"
    """The name of the convert_values column in the column_convert table
    (`str`)
    """
    COLCON_TABLE_COLUMNS = [
        COLCON_IDX,
        COL_COLUMN_IDX,
        COLCON_STR,
        COLCON_VALUES
    ]
    """The column order in the column_convert table (`list` or `str`)
    """
    CREATE_COLUMN_CONVERT_TABLE = f"""
    CREATE TABLE column_convert (
    {COLCON_IDX} INTEGER PRIMARY KEY,
    {COL_COLUMN_IDX} INTEGER  NOT NULL,
    {COLCON_STR} VARCHAR(255) NOT NULL,
    {COLCON_VALUES} VARCHAR(255)
    )
    """
    """Create table statement for specific conversion parameters for column
    data, this is not implemented but is in place just in case (`str`)
    """

    META_IDX = "metadata_idx"
    """The name of the primary key column in the metadata table (`str`)
    """
    META_VERSION = "version"
    """The name of the file-indexer package version column in the metadata
    table (`str`)
    """
    META_FILE_SIZE = "filesize"
    """The name of the filesize column in the metadata table (`str`)
    """
    META_FILE_ROWS = "file_rows"
    """The name of the file_rows column in the metadata table (`str`)
    """
    META_INDEX_ROWS = "index_rows"
    """The name of the index_rows column in the metadata table (`str`)
    """
    META_COMMENT = "comment"
    """The name of the comment column in the metadata table (`str`)
    """
    META_SKIP_LINES = "skiplines"
    """The name of the skiplines column in the metadata table (`str`)
    """
    META_IDX_LEN = "index_length"
    """The name of the index_length column in the metadata table (`str`)
    """
    META_DELIMITER = "delimiter"
    """The name of the delimiter column in the metadata table (`str`)
    """
    META_TABLE_COLUMNS = [
        META_IDX,
        META_VERSION,
        META_FILE_SIZE,
        META_FILE_ROWS,
        META_INDEX_ROWS,
        META_COMMENT,
        META_SKIP_LINES,
        META_IDX_LEN,
        META_DELIMITER
    ]
    """The column order in the metadata table (`list` or `str`)
    """
    CREATE_METADATA_TABLE = f"""
    CREATE TABLE metadata (
    {META_IDX} INTEGER PRIMARY KEY,
    {META_VERSION} VARCHAR(50) NOT NULL,
    {META_FILE_SIZE} UBIGINT NOT NULL,
    {META_FILE_ROWS} UBIGINT NOT NULL,
    {META_INDEX_ROWS} UBIGINT NOT NULL,
    {META_COMMENT} VARCHAR(100),
    {META_SKIP_LINES} INTEGER NOT NULL,
    {META_IDX_LEN} UBIGINT NOT NULL,
    {META_DELIMITER} VARCHAR(10) NOT NULL
    )
    """
    """Create table statement for the indexed file metadata (`str`)
    """

    CREATE_INDEX_TABLE = f"""
    CREATE TABLE indexed (
    {ROW_IDX_COL_NAME} UBIGINT PRIMARY KEY,
    {IDX_VAL_COL_NAME} {{0}},
    {SEEK_COL_NAME} UBIGINT NOT NULL,
    {SEEK_END_COL_NAME} UBIGINT NOT NULL,
    {LINE_RUN_COL_NAME} UBIGINT NOT NULL
    )
    """
    """Create table statement for the index values and seek positions (`str`)
    """

    CREATE_UNICODE_TABLE = """
    CREATE TABLE unicode_convert (
    unicode_convert_pk INTEGER PRIMARY KEY,
    row_no BIGINT NOT NULL,
    col_no INT NOT NULL,
    start_idx INT NOT NULL,
    end_idx INT NOT NULL,
    start_str VARCHAR(1000),
    end_str VARCHAR(1000)
    )
    """
    """Create table statement for any unicode conversion issues (`str`)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, index_path, tmpdir=None, read_only=True):
        self.index_path = str(utils.get_full_path(index_path))
        self.read_only = read_only
        self.tmpdir = tmpdir
        self.executor = None
        self._metadata = None
        self._column_data = None
        self._index_col = dict()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """Connect to an index database.
        """
        pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Disconnect from an index database.
        """
        self.executor = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def create_tables(self, index_col, index_length=None):
        """Create all the required tables in the index database. This requires
        ``read_only=False``.

        Parameters
        ----------
        index_col : `sqlalchemy_config.orm_code.FileColumn`
            The index column object.
        index_length : `int`, optional, default: `NoneType`
            The required length for the datatype of the index value column. If
            not defined the the ``max_len`` attribute of ``index_col`` is used
            for the ``index_length``.
        """
        dtype = self.get_sql_datatype(index_col, index_length)

        # Create the metadata  and column attribute tables.
        self.executor.execute(self.CREATE_METADATA_TABLE)
        self.executor.execute(self.CREATE_COLUMNS_TABLE)
        self.executor.execute(self.CREATE_COLUMN_CONVERT_TABLE)
        self.executor.execute(self.CREATE_UNICODE_TABLE)

        sql = self.CREATE_INDEX_TABLE.format(dtype)
        self.executor.execute(sql)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_sql_datatype(self, index_col, index_length=None):
        """Get the SQL data type for a column data type.

        Parameters
        ----------
        index_col : `sqlalchemy_config.orm_code.FileColumn`
            The index column object.
        index_length : `int`, optional, default: `NoneType`
            The required length for the datatype of the index value column. If
            not defined the the ``max_len`` attribute of ``index_col`` is used
            for the ``index_length``.

        Returns
        -------
        dtype : `str`
            The SQL data type string.
        """
        index_length = index_length or index_col.max_len

        dtype = None
        if index_col.dtype == index_col.Dtype.FLOAT:
            dtype = "FLOAT"
        elif index_col.dtype == index_col.Dtype.INTEGER:
            dtype = "INTEGER"
        elif index_col.dtype == index_col.Dtype.SMALL_INTEGER:
            dtype = "SMALLINT"
        elif index_col.dtype == index_col.Dtype.BIG_INTEGER:
            dtype = "BIGINT"
        elif index_col.dtype == index_col.Dtype.TEXT:
            dtype = "TEXT"
        elif index_col.dtype == index_col.Dtype.BOOLEAN:
            dtype = "BOOLEAN"
        else:
            dtype = f"VARCHAR({index_length})"
        return dtype

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def import_indexed_metadata(self, file_size, file_rows, index_rows,
                                skiplines, comment, index_length, delimiter):
        """Load the indexed file metadata into the database.

        Parameters
        ----------
        file_size : `int`
            The size of the file being indexed in bytes.
        file_rows : `int`
            The number of rows in the file being indexed.
        index_rows : `int`
            The number of rows in the index.
        skiplines : `int`
            The number of lines that are needed to be skipped before reading
            the ``indexed_file`` header.
        comment : `str`
            The comment character(s). Lines in the indexed file that start with
            comment characters are skipped.
        index_length : `int`
            The length for the datatype of the index value column.
        delimiter : `str`
            The delimiter of the indexed file.

        Notes
        -----
        This assumes that the table has already been created and is empty.
        """
        col_str = ",".join(self.META_TABLE_COLUMNS)
        self.executor.execute(
            f"""
            INSERT INTO metadata({col_str})
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)
            """,
            (
                1, __version__, file_size, file_rows, index_rows, comment,
                skiplines, index_length, delimiter
            )
        )
        self.conn.commit()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def import_column_definitions(self, col_info):
        """Load the indexed file column data into the database.

        Parameters
        ----------
        col_info : `sqlalchemy_config.orm_code.FileColumn`
            The index column information object.

        Notes
        -----
        This assumes that the ``column`` and ``column_convert`` table have
        already been created and are empty.
        """
        col_str = ",".join(self.COLUMN_TABLE_COLUMNS)
        column_sql = f"""
        INSERT INTO columns({col_str})
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        """
        con_str = ",".join(self.COLCON_TABLE_COLUMNS)
        convert_sql = f"""
        INSERT INTO column_convert({con_str})
        VALUES (?, ?, ?, ?)
        """
        cc_idx = 1
        for idx, i in enumerate(col_info.columns):
            self.executor.execute(
                column_sql,
                (
                    idx,
                    i.name,
                    i.dtype.value,
                    i.max_len,
                    i.ntests,
                    i.nvalued_tests,
                    i.ascending,
                    i.descending,
                    i.nullable,
                    i.index
                )
            )
            if i.dtype in [i.Dtype.DATE, i.Dtype.DATE_TIME]:
                for dc in i.date_converters:
                    self.executor.execute(
                        convert_sql,
                        (cc_idx, idx, dc, None)
                    )
                    cc_idx += 1
        self.conn.commit()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def import_index_file(self, index_file, index_col, commit_every=10000,
                          verbose=False):
        """Load a temporary index file into the index database.

        Parameters
        ----------
        index_file : `iterator`
            An iterator that will yield rows for the index table.
        index_col : `sqlalchemy_config.orm_code.FileColumn`
            The index column object, with data type info max length.
        commit_every : `int`, optional, default: `10000`
            Buffer this many rows into memory before a batch load/commit.
        verbose : `bool`, optional, default: `False`
            Show the progress of the database load.

        Returns
        -------
        file_rows : `int`
            The number of rows in the file being indexed.
        index_rows : `int`
            The number of rows in the index.

        Raises
        ------
        ValueError
            If the index file header does not match the expected header.

        Notes
        -----
        This assumes that the table has already been created and is empty. It
        will also create a database index on the index value column after the
        load is complete. The index file is expected to be gzipped compressed
        and tab delimited.
        """
        return 0, 0

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_metadata(self):
        """Get all the metadata for an index.

        Returns
        -------
        metadata : `dict`
            The metadata for the index.

        Raises
        ------
        ValueError
            If the number of metadata rows != 1.
        """
        sql = "SELECT * FROM metadata"
        rows = self.executor.execute(sql).fetchall()
        if len(rows) != 1:
            raise ValueError(
                f"expected a single row of metadata not: {len(rows)}"
            )
        return dict(zip(self.META_TABLE_COLUMNS, rows[0]))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def index_col(self):
        """
        """
        if len(self._index_col) == 0:
            for row in self.get_columns():
                if row[self.COL_IS_INDEX] is True:
                    self._index_col = row
                    return row
        return self._index_col

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_columns(self):
        """Get all the columns in the data file represented by the index.

        Returns
        -------
        columns : `list` of `dict`
            The columns, they are returned in the same order that they occur in
            the index file.
        """
        sql = "SELECT * FROM columns"

        columns = []
        for i in self.executor.execute(sql).fetchall():
            row = dict(zip(self.COLUMN_TABLE_COLUMNS, i))
            if row[self.COL_IS_INDEX] is True:
                self.index_col = row
            columns.append(row)
        return columns

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def prepare_index_query(self, queries, file_order=False):
        """Prepare the index query SQL.

        Parameters
        ----------
        queries : `list` of `tuples`
            One or more things to query. Each tuple is a query operator at 0
            and the query value at 1. Valid query operators are: ``=``, ``==``,
            ``=~`` (regex), ``>=``, ``<=``, ``>``, ``<``, ``LIKE``, ``BETWEEN``
            . The regexp operator can have an additional flag at position 2 in
            the tuple indicating any regex flags, such as ``i`` for case
            insensitive. Please note the regex is not valid for SQLite indexes
            and is mapped to a LIKE operator. Additionally, a ``WHERE``
            operator can be given. In this case, the query value at one is
            expected to be a formatted where clause for an SQL query, relevant
            for the index database type.
        file_order : `bool`, optional, default: `False`
            Return the results in the file order (seek position) rather than
            the index order.

        Returns
        -------
        sql : `str`
            The SQL query string.
        """
        col_name = IDX_VAL_COL_NAME
        query_components = []
        query_parameters = []
        for i in queries:
            try:
                op, val = i
                flags = None
            except ValueError:
                op, val, flags = i

            op = op.upper().strip()
            if op in ('==', '='):
                query_components.append(
                    f"{col_name} = ?"
                )
                query_parameters.append(val)
            elif op == '=~':
                q, val = self.get_regex(
                    col_name, val, flags=flags
                )
                query_components.append(q)
                query_parameters.append(val)
            elif op == '>=':
                query_components.append(
                    f"{col_name} >= ?"
                )
                query_parameters.append(val)
            elif op == '<=':
                query_components.append(
                    f"{col_name} <= ?"
                )
                query_parameters.append(val)
            elif op == '>':
                query_components.append(
                    f"{col_name} > ?"
                )
                query_parameters.append(val)
            elif op == '<':
                query_components.append(
                    f"{col_name} < ?"
                )
                query_parameters.append(val)
            elif op == 'BETWEEN':
                query_components.append(
                    f"{col_name} >= ? AND {col_name} <= ?"
                )
                query_parameters.append(val)
                query_parameters.append(flags)
            elif op == 'WHERE':
                query_components.append(
                    re.sub(r'^WHERE', op)
                )
            elif op == 'LIKE':
                query_components.append(f"{col_name} LIKE ?")
                query_parameters.append(val)
            else:
                # Plain equality
                query_components.append(
                    f"{col_name} = ?"
                )
                query_parameters.append(val)

        sql = "SELECT * FROM indexed WHERE {0}".format(
            " OR ".join([f"({i})" for i in query_components])
        )
        if file_order is True:
            sql = sql + f" ORDER BY {SEEK_COL_NAME}"
        return sql, query_parameters

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def query_index(self, queries, **kwargs):
        """Query the index file. Sub-classes should override this.

        Parameters
        ----------
        queries : `list` of `tuples`
            One or more things to query. Each tuple is a query operator at 0
            and the query value at 1. Valid query operators are: ``=``, ``==``,
            ``=~`` (regex), ``>=``, ``<=``, ``>``, ``<``, ``LIKE``, ``BETWEEN``
            . The regexp operator can have an additional flag at position 2 in
            the tuple indicating any regex flags, such as ``i`` for case
            insensitive. Please note the regex is not valid for SQLite indexes
            and is mapped to a LIKE operator. Additionally, a ``WHERE``
            operator can be given. In this case, the query value at one is
            expected to be a formatted where clause for an SQL query, relevant
            for the index database type.
        **kwargs
            Any additional keyword arguments.
        """
        pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SqliteIndex(_BaseIndex):
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """Connect to an SQLite index database.
        """
        raise NotImplementedError("SQLite index is currently disabled")
        uri = f'file:{self.index_path}'
        if self.read_only is True:
            uri = f'{uri}?mode=ro'

        self.conn = sqlite3.connect(uri, uri=True)
        self.cur = self.conn.cursor()
        self.executor = self.cur

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Close the SQLite index connection.
        """
        self.conn.close()
        super().close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def import_index_file(self, index_file, index_col, commit_every=10000,
                          verbose=False):
        """Load a temporary index file into the index database.

        Parameters
        ----------
        index_file : `iterator`
            An iterator that will yield rows for the index table.
        index_col : `sqlalchemy_config.orm_code.FileColumn`
            The index column object, with data type info max length.
        commit_every : `int`, optional, default: `10000`
            Buffer this many rows into memory before a batch load/commit.
        verbose : `bool`, optional, default: `False`
            Show the progress of the database load.

        Returns
        -------
        file_rows : `int`
            The number of rows in the file being indexed.
        index_rows : `int`
            The number of rows in the index.

        Raises
        ------
        ValueError
            If the index file header does not match the expected header.

        Notes
        -----
        This assumes that the table has already been created and is empty. It
        will also create a database index on the index value column after the
        load is complete. The index file is expected to be gzipped compressed
        and tab delimited.
        """
        sql = """
        INSERT INTO indexed(idx_pk, idx_value, seek, line_run)
        VALUES (?, ?, ?, ?)
        """

        tqdm_kwargs = dict(
            unit=" rows", desc="[info] creating index", disable=not verbose
        )

        header = next(index_file)
        if header != INDEX_HEADER:
            raise ValueError("unexpected header in index file")

        # Get the python data type for the index
        dtype = index_col.Dtype.get_python_cast(index_col.dtype)

        # A counter for the number of rows buffered and the list buffer
        row_buffer = []
        file_rows = 0
        index_rows = 0
        for row in tqdm(index_file, **tqdm_kwargs):
            # cast the index value to the required datatype, I have to be
            # explicit here as I do not want to make a str(None), I want true
            # NULL values to be represented as is, I might change this to empty
            # strings
            if row[IDX_VAL_COL_IDX] is not None:
                val = dtype(row[IDX_VAL_COL_IDX])
            idx = int(row[ROW_IDX_COL_IDX])
            seek = int(row[SEEK_COL_IDX])
            line_run = int(row[LINE_RUN_COL_IDX])
            index_rows += 1
            file_rows += line_run

            # Add to the buffer
            row_buffer.append((idx, val, seek, line_run))
            # Is the buffer big enough to commit
            if len(row_buffer) == commit_every:
                # Commit and reset
                self.executor.executemany(sql, row_buffer)
                self.conn.commit()
                row_buffer = []

        # Final commit if stuff is still in the row buffer
        if len(row_buffer) > 1:
            self.executor.executemany(sql, row_buffer)
            self.conn.commit()

        # Now we index the indexed column for searching
        self.executor.execute(
            "CREATE INDEX idx_value_idx ON indexed(idx_value)"
        )
        return file_rows, index_rows

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_regex(self, col, value, **kwargs):
        """Get a regular expression for SQLite, this just returns a LIKE
        statement.

        Parameters
        ----------
        col : `str`
            The column name being queried.
        value : `str`
            The LIKE statement to query for.
        **kwargs
            Ignored.

        Returns
        -------
        regexp_operator : `str`
            A SQLite LIKE statement
        """
        if not value.startswith("%"):
            value = f"%{value}"

        if not value.endswith("%"):
            value = f"{value}%"

        return f"{col} LIKE ?", value

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def query_index(self, queries, **kwargs):
        """Query the index file. Sub-classes should override this.

        Parameters
        ----------
        queries : `list` of `tuples`
            One or more things to query. Each tuple is a query operator at 0
            and the query value at 1. Valid query operators are: ``=``, ``==``,
            ``=~`` (regex), ``>=``, ``<=``, ``>``, ``<``, ``LIKE``, ``BETWEEN``
            . The regexp operator can have an additional flag at position 2 in
            the tuple indicating any regex flags, such as ``i`` for case
            insensitive. Please note the regex is not valid for SQLite indexes
            and is mapped to a LIKE operator. Additionally, a ``WHERE``
            operator can be given. In this case, the query value at one is
            expected to be a formatted where clause for an SQL query, relevant
            for the index database type.
        **kwargs
            Any additional keyword arguments passed to
            ``SqliteIndex.prepare_index_query``.
        """
        sql = self.prepare_index_query(queries, **kwargs)
        for row in self.executor.execute(sql):
            yield row


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DuckDbIndex(_BaseIndex):
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """Connect to an index database. This is just a wrapper around
        ``duckdb.connect``.
        """
        self.conn = duckdb.connect(
            database=self.index_path, read_only=self.read_only,
            # Case sensitive
            config={'default_collation': 'BINARY'}
        )
        self.executor = self.conn

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Connect to an index database.
        """
        self.conn.close()

        try:
            os.unlink(self._tmpfile)
        except AttributeError:
            pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def import_index_file(self, index_file, index_col, commit_every=10000,
                          verbose=False, encoding='utf8'):
        """Load a temporary index file into the index database. Note that rows
        are loaded with an internal duckdb load function.

        Parameters
        ----------
        index_file : `iterator`
            An iterator that will yield rows for the index table.
        index_col : `sqlalchemy_config.orm_code.FileColumn`
            The index column object, with data type info max length.
        commit_every : `int`, optional, default: `10000`
            Ignored.
        verbose : `bool`, optional, default: `False`
            Show the progress of the database load. Only applies to initial
            data processing not to the actual load that is performed by duckdb.

        Returns
        -------
        file_rows : `int`
            The number of rows in the file being indexed.
        index_rows : `int`
            The number of rows in the index.

        Raises
        ------
        ValueError
            If the index file header does not match the expected header.

        Notes
        -----
        This assumes that the table has already been created and is empty. It
        will also create an index on the index value column after the load is
        complete. The index file is expected to be gzipped compressed and tab
        delimited.

        The data being loaded must be castable to UTF-8, note that this is
        different from the actual index file which can is latin encoded.
        """
        tqdm_kwargs = dict(
            unit=" rows", desc="[info] creating index", disable=not verbose
        )

        dtype = self._get_write_cast_func(index_col)

        # Sanity check the header of the index file
        header = next(index_file)
        if header != INDEX_HEADER:
            raise ValueError("unexpected header in index file")

        file_rows = 0
        index_rows = 0
        # idx = 1
        self._tmpfile = utils.get_temp_file(dir=self.tmpdir)
        with gzip.open(self._tmpfile, 'wt', encoding=encoding) as tmpout:
            writer = csv.writer(tmpout, delimiter="\t")
            for row in tqdm(index_file, **tqdm_kwargs):
                # row = [i if i not in _MISSING else None for i in row]
                row[IDX_VAL_COL_IDX] = dtype(row[IDX_VAL_COL_IDX])
                # pp.pprint(row)
                line_run = int(row[LINE_RUN_COL_IDX])
                index_rows += 1
                file_rows += line_run
                writer.writerow(row)
                # idx += 1

        # with gzip.open(self._tmpfile, 'rt', encoding='latin') as tmpout:
        #     reader = csv.reader(tmpout, delimiter="\t")
        #     for row in reader:
        #         pp.pprint(row)

        # Now issue the duckdb data load command
        try:
            self.executor.execute(
                f"""COPY indexed FROM '{self._tmpfile}'
                (
                HEADER 'FALSE',
                DELIMITER '\t',
                COMPRESSION 'gzip',
                ENCODING 'UTF8'
                );
                """
            )
        except Exception as e:
            debug = utils.get_temp_file(
                dir=os.environ['HOME'],
                prefix="fidx-build_"
            )
            # print(debug)
            shutil.move(
                self._tmpfile,
                debug
            )
            raise e.__class__(
                str(e)+f": index written to: {debug}"
            ) from e

        # Now we index the indexed column for searching
        self.executor.execute(
            "CREATE INDEX idx_value_idx ON indexed(idx_value)"
        )
        return file_rows, index_rows

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_regex(self, col, value, flags=None):
        """Get a regular expression for DuckDB.

        Parameters
        ----------
        col : `str`
            The column name being queried.
        value : `str`
            The LIKE statement to query for.
        flags : `str`, optional, default: `NoneType`
            Any flags for the regular expression operator, such as ``i`` for
            case insensitive.

        Returns
        -------
        regexp_operator : `str`
            A duckdb ``regexp_matches`` statement
        """
        if flags is None:
            return f"regexp_matches({col}, ?)", value
        else:
            return f"regexp_matches({col}, ?, '{flags}')", value

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def query_index(self, queries, **kwargs):
        """Query the index file. Sub-classes should override this.

        Parameters
        ----------
        queries : `list` of `tuples`
            One or more things to query. Each tuple is a query operator at 0
            and the query value at 1. Valid query operators are: ``=``, ``==``,
            ``=~`` (regex), ``>=``, ``<=``, ``>``, ``<``, ``LIKE``, ``BETWEEN``
            . The regexp operator can have an additional flag at position 2 in
            the tuple indicating any regex flags, such as ``i`` for case
            insensitive. Please note the regex is not valid for SQLite indexes
            and is mapped to a LIKE operator. Additionally, a ``WHERE``
            operator can be given. In this case, the query value at one is
            expected to be a formatted where clause for an SQL query, relevant
            for the index database type.
        **kwargs
            Any additional keyword arguments passed to
            ``DuckDbIndex.prepare_index_query``.
        """
        sql, parameters = self.prepare_index_query(queries, **kwargs)
        res = self.executor.execute(sql, parameters)

        row = res.fetchone()
        while row is not None:
            yield row
            row = res.fetchone()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_write_cast_func(self, index_col):
        """
        """
        parse_func = index_col.Dtype.get_python_cast(
            index_col.dtype, date_formats=index_col.date_converters
        )

        if index_col.dtype in [index_col.Dtype.DATE, index_col.Dtype.DATE_TIME]:
            return _get_date_format(parse_func)
        elif index_col.dtype == index_col.Dtype.BOOLEAN:
            return _get_bool_format(parse_func)
        return str


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_date_format(func):
    def _format_date(x):
        if x == '':
            return x
        x = func(x)
        return x.strftime("%Y-%m-%d")
    return _format_date


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_bool_format(func):
    def _format_bool(x):
        if x == '':
            return x
        x = func(x)
        return int(x)
    return _format_bool
