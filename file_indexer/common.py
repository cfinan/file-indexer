"""Common functions used by multiple modules.
"""
from pathlib import PosixPath
from pyaddons import utils
from Bio import bgzf
from glob import glob


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BgzfEncodingReader(bgzf.BgzfReader):
    """A wrapper around the biopython ``bgzf.BgzfReader``, this applies
    encoding onto data read in bytes.

    Parameters
    ----------
    *args
        arguments to ``Bio.bgzf.BgzfReader``.
    encoding : `str`, optional, default: `UTF8`
        The encoding of the returned lines of data. This applies to rows read
        in with the ``readline`` method,
    **kwargs
        Any keyword arguments to ``Bio.bgzf.BgzfReader``

    Notes
    -----
    This will always open the underlying file in binary mode and the encoding
    is applied to each full line that is read in. Therefore, this allows UTF8
    encoding to be applied before the line being consumed by something like
    ``csv.reader``.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *args, encoding='UTF8', **kwargs):
        if len(args) > 1:
            args = list(args)
            args.pop(1)
        kwargs.pop('mode', None)
        super().__init__(*args, mode='rb', **kwargs)
        self._file_encoding = encoding

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def readline(self):
        """Read a line in from a bgzipped file and apply the encoding.

        Returns
        -------
        row : `str`
            A row encoded as ``encoding``.
        """
        line = super().readline()
        return line.decode(self._file_encoding)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class BgzfEncodingWriter(bgzf.BgzfWriter):
    """A wrapper around the biopython ``bgzf.BgzfWriter``, this converts
    encoded rows to bytes before writing.

    Parameters
    ----------
    *args
        arguments to ``Bio.bgzf.BgzfWriter``.
    **kwargs
        Any keyword arguments to ``Bio.bgzf.BgzfWriter``

    Notes
    -----
    This will always open the underlying file in binary mode and expects data
    to be given as some sort of encoded string (not bytes).
    """
    def __init__(self, *args, **kwargs):
        if len(args) > 1:
            args.pop(1)
        kwargs.pop('mode', None)
        super().__init__(*args, mode='wb', **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def write(self, data):
        """Write some data to a bgzf compressed row.

        Parameters
        ----------
        data : `str`
            Some data to write. The data is encoded to bytes before writing.
        """
        super().write(data.encode())


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_indexes(indexed_file, column=None):
    """Attempt to locate the indexes for a (potentially) indexed file.

    Parameters
    ----------
    indexed_file : `str`
        The path to the potentially indexed file.
    column : `str`, optional, default: NoneType
        An optional column name for the index. This is useful if multiple index
        files have been created on different column and you only want indexes
        matching those columns returned.

    Returns
    -------
    index_files : `list` of `str`
        The path to index files that match the ``indexed_file`` structure.

    Raises
    ------
    IOError
        If no index files can be found for the ``indexed_file``.
    """
    no_ext = utils.strip_file_extension(indexed_file)
    index_files = glob(f"{no_ext}-*.idx")

    if column is not None:
        index_files = [
            i for i in index_files
            if i == f"{no_ext}-{column}.idx"
        ]

    if len(index_files) == 0:
        raise IOError(f"no index files found for: {indexed_file}")
    return index_files


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_open_method(indexed_file):
    """Determine the open function to use to open the indexed file.

    Parameters
    ----------
    indexed_file : `str`
        The path to the file.

    Returns
    -------
    open_method : `builtins.open` or `bgzf.open`
        The method to use to open the indexed file.

    Raises
    ------
    IOError
        If the indexed file is gzipped but not bgzipped.

    Notes
    -----
    This only tests for (b)gzip compression. If it is not compressed then
    the assumption is that it is plain text.
    """
    if indexed_file is not None and \
       isinstance(indexed_file, (str, PosixPath)) is False:
        if hasattr(indexed_file, '__iter__') is False:
            raise TypeError(
                "expected a file path, STDIN or an iterator:"
                f" {type(indexed_file)}"
            )
        return open

    is_gzipped = utils.is_gzip(indexed_file)
    is_bgzipped = utils.is_bgzip(indexed_file)

    if is_gzipped is True and is_bgzipped is False:
        raise IOError(
            "index files must be plain texted or bgzip compressed, not"
            " gzip compressed"
        )

    if is_bgzipped is True:
        # print("BGZIPPED")
        # return bgzf.open
        return BgzfEncodingReader
    # print("RAW")
    return open
