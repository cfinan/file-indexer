"""Create index on a column of a file. The file must be tabular and have a
header row. The file can be a plain text file or you can compress with bgzip.
You also have the option of giving an input file or STDIN and creating a
BGZIPPED output file that will be used to create an index.

Please note that the BGZIPPED output files do not support multi-byte
characters. These will be converted using unidecode and any conversion
will be written to a log file.
"""
from file_indexer import (
    __version__,
    __name__ as pkg_name,
    database,
    common
)
from pyaddons import log, utils
from pyaddons.flat_files import header
from sqlalchemy_config.orm_code import ColumnInfo
from tqdm import tqdm
from pathlib import Path, PosixPath
from merge_sort import chunks, merge
import datetime
import stdopen
import argparse
import sys
import os
import csv
import re
import shutil
import tempfile
import gzip
import math
# import pprint as pp


_SCRIPT_NAME = "fidx-build"
"""The name of the script (`str`)
"""
# Use the module docstring for argparse
_DESC = __doc__
"""The program description (`str`)
"""
_STDIN_SYMBOLS = [None, '-']
"""Indicators that input/output is via STDIN/STDOUT (`list`)
"""
_INDEX_DELIMITER = "\t"
"""The delimiter of the temp index file (`str`)
"""
_INDEX_ENCODING = 'utf8'
"""The encoding of the index (`str`)
"""
DUCKDB_INDEX = "duckdb"
"""The DuckDB index type (`str`)
"""
SQLITE_INDEX = "sqlite"
"""The SQLite index type (`str`)
"""
VALUE_ORDER = 'value'
"""The name for the option of the index being sorted in index value order
(`str`)
"""
FILE_ORDER = 'file'
"""The name for the option of the index being sorted in file order (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API use see
    ``file_indexer.build.build_index``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    try:
        # Initiate index building
        build_index(args.infile, args.index_col, outfile=args.outfile,
                    delimiter=args.delimiter, encoding=args.encoding,
                    comment=args.comment, tmpdir=args.tmpdir,
                    skiplines=args.skip_lines, index_file=args.index_file,
                    verbose=args.verbose, index_length=args.index_length,
                    overwrite=args.overwrite, index_type=args.index_type,
                    index_order=args.index_order)
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        log.log_end(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        'index_col', type=str, help="The name of the column to index."
    )
    parser.add_argument(
        '-i', '--infile',
        type=str,
        help="The input file you want to index. It is expected that the input"
        " file is sorted (or at least grouped) on the column that you want to"
        " index on. If the input file is compressed it should be with BGZIP "
        "not GZIP."
    )
    parser.add_argument(
        '-o', '--outfile',
        type=str,
        help="A processed output file. If you want to use the input file"
        " directly then do not supply an output file name. If supplied the"
        " output file will be written with BGZIP compression."
    )
    parser.add_argument(
        '-T', '--tmpdir',
        type=str,
        help="File output is written via temp, you can change the temp"
        " directory here. The default is to use the system tmp location."
    )
    parser.add_argument(
        '-d', '--delimiter', type=str, default="\t",
        help="The delimiter of the input file."
    )
    parser.add_argument(
        '-e', '--encoding', type=str, default="UTF8",
        help="The encoding of the input file."
    )
    parser.add_argument(
        '-s', '--skip-lines', type=int, default=0,
        help="The skip this many lines before processing the header."
    )
    parser.add_argument(
        '-L', '--index-length', type=int,
        help="The index length in characters, this is applied to both string"
        " and int indexes. The default is to use the maximum length value in"
        " the index column."
    )
    parser.add_argument(
        '--index-file', type=str,
        help="An alternative name for the index file. The default is to use"
        " the name of the file being indexed joined to the indexing column"
        " name with a hyphen. The indexing column name is preprocessed to"
        " make lowercase, remove spaces and non-word characters."
    )
    parser.add_argument(
        '--index-type', type=str, default=DUCKDB_INDEX,
        choices=[DUCKDB_INDEX, SQLITE_INDEX],
        help="The type of file based database to use for the index file."
        "SQLite indexes are currently disabled"
    )
    parser.add_argument(
        '-c', '--comment', type=str, default="#",
        help="Skip lines that start with this string."
    )
    parser.add_argument(
        '-v', '--verbose', action="count",
        help="Give more output, use -vv to turn on progress monitoring."
    )
    parser.add_argument(
        '--index-order', default=FILE_ORDER, choices=[FILE_ORDER, VALUE_ORDER],
        help="The order the index file is written in. Either file row "
        "occurrence order or Index value sort order."
    )
    parser.add_argument(
        '-O', '--overwrite', action="store_true",
        help="Overwrite any existing index files. The default behaviour is"
        " to raise a FileExistsError if an index file with the same name"
        " already exists."
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments.
    """
    args = parser.parse_args()
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_index(infile, index_col, outfile=None, delimiter="\t",
                encoding="utf8", comment='#', tmpdir=None, skiplines=None,
                index_file=None, overwrite=False, verbose=False,
                index_length=None, index_type=None, index_order='file',
                out_delimiter=None):
    """Build in index on a columnar file (or STDIN).

    Parameters
    ----------
    infile : `str` or `NoneType`
        The input file for which you want to index a column. If it is NoneType
        or ``-``, then input is assumed to be from STDIN. The input file must
        have a header line.
    index_col : `str`
        The name of the column to index. Must exist in the header line and be
        unique.
    outfile : `str`, optional, default: `NoneType`
        The output file name. If not supplied then it is assumed that the input
        does not need processing and it will be used directly to make the
        index. If supplied, then a new file will be written, which will be
        written as a BGZIPED output and that will be used to form the index.
        The output file will also be written using LATIN encoding and any
        non-conforming unicode will be converted if possible, or removed and
        written to a log file.
    delimiter : `str`, optional, default: `\t`
        The delimiter of the input.
    encoding : `str`, optional, default: `utf8`
        The encoding of the input file.
    comment : `str`, optional, default: `#`
        Line comment characters in the input file. Lines starting with these
        are skipped. Comments are not written to any output files.
    tmpdir : `str`, optional, default: `NoneType`
        Output files are written via tmp. This is the location of the temp
        directory. If not supplied then the default system tmp location is
        used.
    skiplines : `int`, optional, default: `NoneType`
        A fixed number of lines to skip at the start of the file, after which
        the header (or maybe comment lines) is expected to be found. Skipped
        rows are not checked for comment characters. Skipped rows are not
        written to any output files.
    index_file : `str`, optional, default: `NoneType`
        The path/name of the index file. If not supplied then the column name
        of the lowercased index column will be used after removing spaces and
        non-word (in the regexp sense) characters.
    overwrite : `bool`, optional, default: `False`
        Overwrite any existing index files. The default behaviour is to raise a
        ``FileExistsError`` if an existing index file is found in the same
        directory as the output file.
    verbose : `bool` or `int`, optional, default: `False`
        Report index building process. Use an int > 1 for process monitoring.
    index_length : `int`, optional, default: `NoneType`
        A positive integer for the length of the index values (in character).
        This is also applied to integer values. If it is ``NoneType`` then the
        value with the maximum length in the index column is used for index
        length.
    index_type : `str`, optional, default: `NoneType`
        The backend to use for the index file. Should be either ``duckdb`` or
        ``sqlite``.
    out_delimiter : `str`, optional, default: `NoneType`
        The delimiter of the output file if one is being created. If not
        supplied then the delimiter will be the same as the input file.

    Returns
    -------
    indexed_file_path : `str`
        The path to the file that has been indexed.
    index_path : `str`
        The path to the index file.

    Raises
    ------
    ValueError
        If infile is via STDIN and outfile is not defined. If the
        ``index_length <= 0`` (and not ``NoneType``). If the index column
        can't be found in the file header row. Or if the index_type is not
        ``duckdb`` or ``sqlite``.
    FileExistsError
        If the index file already exists and ``overwrite`` is ``False``.
    """
    # Make sure the index order argument is valid
    if index_order not in [FILE_ORDER, VALUE_ORDER]:
        raise ValueError(f"unknown index order: {index_order}")

    # Determine the index type required by the user
    index_type = index_type or DUCKDB_INDEX
    skiplines = skiplines or 0
    # build_class = None

    if index_type == DUCKDB_INDEX:
        build_class = database.DuckDbIndex
    elif index_type == SQLITE_INDEX:
        raise NotImplementedError(
            "SQLite indexes are disabled at the moment."
        )
        # build_class = database.SqliteIndex
    else:
        raise ValueError(f"unknown index type: {index_type}")

    # Do we want progress monitors
    prog_verbose = log.progress_verbose(verbose=verbose)
    logger = log.retrieve_logger(_SCRIPT_NAME, verbose=verbose)

    # Make sure that an output file is defined if input is from STDIN
    if infile in _STDIN_SYMBOLS and outfile in _STDIN_SYMBOLS:
        raise ValueError("input via STDIN must have an output file")

    # Sanity check the index length
    if index_length is not None and index_length <= 0:
        raise ValueError("The index length must be None or > 0")

    # Get full path of the input file
    if infile not in _STDIN_SYMBOLS and \
       isinstance(infile, (str, Path, PosixPath)):
        infile = utils.get_full_path(infile)

    # Create a working temp directory
    working_temp = tempfile.mkdtemp(dir=tmpdir)

    temp_indexed, temp_idx, info = None, None, None
    indexed_file = None
    try:
        # If an output file is specified, then we want to read the input source
        # and write to a bgzipped output file (via temp), which will be indexed
        # as we write.
        if outfile not in _STDIN_SYMBOLS:
            # Get full path of the output file
            outfile = utils.get_full_path(outfile)

            # Sanity check that the user has not specified the same
            # input/output file
            if outfile == infile:
                raise FileExistsError(
                    "output file is the same as the input file"
                )

            try:
                # Does the output file already exist?
                open(outfile).close()
                if overwrite is False:
                    raise FileExistsError(
                        "output file already exists and overwrite is False"
                    )
            except FileNotFoundError:
                pass

            logger.info("indexing from STDIN")

            # Set the output delimiter (if needed)
            out_delimiter = out_delimiter or delimiter

            # Now build the index
            temp_indexed, temp_idx, final_index, info = _index_output(
                infile, index_col, outfile, delimiter=delimiter,
                encoding=encoding, comment=comment, tmpdir=working_temp,
                skiplines=skiplines, index_file=index_file,
                overwrite=overwrite, verbose=prog_verbose,
                index_length=index_length, out_delimiter=out_delimiter
            )
            # The output file does not have any rows to skip, even if the input
            # file did
            skiplines = 0
            indexed_file = temp_indexed

            # Make sure the correct delimiter is stored in the database
            delimiter = out_delimiter
        else:
            logger.info("indexing from input file")

            # Index the input file
            temp_idx, final_index, info = _index_input(
                infile, index_col, delimiter=delimiter, encoding=encoding,
                comment=comment, tmpdir=working_temp, skiplines=skiplines,
                index_file=index_file, overwrite=overwrite,
                verbose=prog_verbose, index_length=index_length
            )
            indexed_file = infile

        logger.info("sorting the index")

        merge_gen = None
        if index_order == VALUE_ORDER:
            # Now we have an index file, we will sort it on the index column
            chunk_files = _sort_index(
                temp_idx, info, tmpdir, verbose=prog_verbose
            )

            # Use a generator to yield sorted file into the index database load
            # objects
            merge_gen = _merge_generator(chunk_files, info, tmpdir)
        else:
            # Use a generator to yield sorted file into the index database load
            # objects
            merge_gen = _index_generator(temp_idx)

        logger.info(f"building the index db: {index_type}")

        # Now build the index database file
        index_db = _build_index_db(
            temp_idx, indexed_file, merge_gen, info, index_length,
            skiplines, comment, delimiter, tmpdir=working_temp,
            verbose=prog_verbose, build_class=build_class
        )
        shutil.move(index_db, final_index)
        if outfile not in _STDIN_SYMBOLS:
            shutil.move(indexed_file, outfile)
            # print("SIZE")
            # print(os.path.getsize(outfile))
        else:
            outfile = indexed_file
    finally:
        # close temp files
        shutil.rmtree(working_temp)
    return outfile, final_index


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _index_output(infile, index_col, outfile, delimiter="\t",
                  encoding="utf8", comment='#', tmpdir=None, skiplines=None,
                  index_file=None, overwrite=False, verbose=False,
                  index_length=None, out_delimiter=None):
    """Read from input and write to BGZIPPED output which is then indexed.

    Parameters
    ----------
    infile : `str` or `NoneType`
        The input file for which you want to index a column. If it is NoneType
        or ``-``, then input is assumed to be from STDIN. The input file must
        have a header line.
    index_col : `str`
        The name of the column to index. Must exist in the reader line.
    outfile : `str`
        The output file name. The oupput file will be written as a BGZIPED
        output and that will be used to form the index. The output file will
        also be written using LATIN encoding and any non-conforming unicode
        will be converted if possible, or removed and written to a log file.
    delimiter : `str`, optional, default: `\t`
        The delimiter of the input.
    encoding : `str`, optional, default: `utf8`
        The encoding of the input file.
    comment : `str`, optional, default: `#`
        Line comment characters in the input file. Lines starting with these
        are skipped. Comments are not written to any output files.
    tmpdir : `str`, optional, default: `NoneType`
        Output files are written via tmp. This is the location of the temp
        directory. If not supplied then the default system tmp location is
        used.
    skiplines : `int`, optional, default: `NoneType`
        A fixed number of lines to skip at the start of the file, after which
        the header (or maybe comment lines) is expeced to be found. Skipped
        rows are not checked for comment characters. Skipped rows are not
        written to any output files.
    index_file : `str`, optional, default: `NoneType`
        The path/name of the index file. If not supplied then the column name
        of the lowercased index column will be used after removing spaces and
        non-word (in the regexp sense) characters.
    overwrite : `bool`, optional, default: `False`
        Overwrite any existing index files. The default behaviour is to raise a
        ``FileExistsError`` if an existing index file is found in the same
        directory as the output file.
    verbose : `bool`, optional, default: `False`
        Report index building process.
    index_length : `int`, optional, default: `NoneType`
        A positive integer for the length of the index values (in character).
        This is also applied to integer values. If it is ``NoneType`` then the
        value with the maximum length in the index column is used for index
        length.
    out_delimiter : `str`, optional, default: `NoneType`
        The delimiter of the output file if one is being created. If not
        supplied then the delimiter will be the same as the input file.

    Returns
    -------
    tempout : `str`
        The path to the temp output file that has been created and indexed.
    tempidx : `str`
        The path to the temporary index file that will eventually be loaded
        into the index database.
    index_file : `str`
        The path to the final index file. This is only the path and will not
        contain the index file at this stage.
    info : `sqlalchemy_config.orm_code.ColumnInfo`
        The column info file specifying the column data types and character
        lengths.

    Raises
    ------
    ValueError
        If the index column can't be found in the file header row.
    """
    # Set up the output delimiter
    out_delimiter = out_delimiter or delimiter

    tqdm_kwargs = dict(
        unit=" rows", desc="[info] indexing output", disable=not verbose
    )

    # Get the temp output file name
    tempout = os.path.join(tmpdir, os.path.basename(outfile))

    # Open the output file
    outfobj = common.BgzfEncodingWriter(tempout)

    # Will eventually hold the file object for provisional writing of the
    # index file
    idxfobj = None
    try:
        # Create the ouptut csv writer
        outwriter = csv.writer(outfobj, delimiter=out_delimiter)

        # Get the method to use to open the input file, this will raise a
        # TypeError if the open method can't be worked out.
        try:
            inopen_method = utils.get_open_method(infile, allow_none=True)
        except TypeError:
            inopen_method = open
            if hasattr(infile, '__iter__') is False:
                raise

        with stdopen.open(infile, mode='rt', method=inopen_method,
                          encoding=encoding) as incsv:
            # Open the input file and get the header row whilst respecting
            # the number of skip lines and comment characters.
            reader = csv.reader(incsv, delimiter=delimiter)

            # TODO: check index column is unique in the header
            header_row, rowno = header.get_header_from_reader(
                reader, skiplines=skiplines, comment=comment
            )

            # Write the header row to the output file
            outwriter.writerow(header_row)

            # Find the index column in the header row
            col_idx = _get_index_col(header_row, index_col)

            # Use the default index file name or generate one
            index_file = index_file or _get_index_file(
                header_row[col_idx], outfile
            )

            # Sanity check to prevent any accidental overwrites
            if index_file == infile or index_file == outfile:
                raise ValueError(
                    "index file name is the same as input or output name"
                )

            # Make sure the index does not exist or if it does and we want
            # to overwrite then we clear it
            _check_index_file(index_file, overwrite)

            # Create a temp index file, open and write the header
            tempidx = os.path.join(tmpdir, os.path.basename(index_file))
            idxfobj = gzip.open(tempidx, 'wt')
            idx_writer = csv.writer(idxfobj, delimiter="\t")
            idx_writer.writerow(database.INDEX_HEADER)

            # This will be used to determine the data types of the columns
            # in the file being indexed
            info = ColumnInfo(header_row)

            # Make sure the index column is flagged as True
            info.columns[col_idx].index = True

            # Get the first data row in the file being indexed
            # incsv is irrelevant here as we are getting the file position from
            # the output file
            first_row, _ = _get_first_row(outfobj, reader, comment)
            info.add_row(first_row)

            # This is the seek position after the header has been written to
            # file
            start_pos = outfobj.tell()
            outwriter.writerow(first_row)

            # Get the index value accounting for the index length a NoneType
            # index_length will return the whole string
            cur_idx = first_row[col_idx][:index_length]

            # When consecutive lines have the same index value we will only
            # store the first one in the index along with the number of
            # additional lines that need to be read in. This means that files
            # sorted on the index column can generate smaller indexes.
            line_run = 1
            row_no = 1
            start_row_no = row_no
            for row in tqdm(reader, initial=1, **tqdm_kwargs):
                if comment is not None and row[0].strip().startswith(comment):
                    continue
                row_no += 1
                info.add_row(row)
                idx = row[col_idx][:index_length]

                # If the current index value is different from the previous one
                # then write the previous one to the index file and set up
                # for the next one
                if idx != cur_idx:
                    old_start_pos = start_pos
                    start_pos = outfobj.tell()
                    idx_writer.writerow(
                        [start_row_no, cur_idx, old_start_pos,
                         start_pos, line_run]
                    )
                    start_row_no = row_no
                    line_run = 0
                    start_pos = outfobj.tell()
                cur_idx = idx
                line_run += 1
                outwriter.writerow(row)
            # Final write
            end_pos = outfobj.tell()
            idx_writer.writerow(
                [start_row_no, cur_idx, start_pos, end_pos, line_run]
            )
    # except TypeError:
    #     # From the input open method detection
    #     if idxfobj is not None:
    #         idxfobj.close()
    #     outfobj.close()
    #     raise NotImplementedError("can't use file objects yet")
    except Exception:
        if idxfobj is not None:
            idxfobj.close()
        outfobj.close()
        raise

    # Everything is now setup to create the SQLite database for the index
    # So close all the files
    idxfobj.close()
    outfobj.close()
    return tempout, tempidx, index_file, info


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _index_input(infile, index_col, delimiter="\t", encoding="utf8",
                 comment='#', tmpdir=None, skiplines=None,
                 index_file=None, overwrite=False, verbose=False,
                 index_length=None):
    """Index the input file.

    Parameters
    ----------
    infile : `str`
        The input file for which you want to index a column.
    index_col : `str`
        The name of the column to index. Must exist in the reader line.
    delimiter : `str`, optional, default: `\t`
        The delimiter of the input.
    encoding : `str`, optional, default: `utf8`
        The encoding of the input file.
    comment : `str`, optional, default: `#`
        Line comment characters in the input file. Lines starting with these
        are skipped. Comments are not written to any output files.
    tmpdir : `str`, optional, default: `NoneType`
        Output files are written via tmp. This is the location of the temp
        directory. If not supplied then the default system tmp location is
        used.
    skiplines : `int`, optional, default: `NoneType`
        A fixed number of lines to skip at the start of the file, after which
        the header (or maybe comment lines) is expeced to be found. Skipped
        rows are not checked for comment characters. Skipped rows are not
        written to any output files.
    index_file : `str`, optional, default: `NoneType`
        The path/name of the index file. If not supplied then the column name
        of the lowercased index column will be used after removing spaces and
        non-word (in the regexp sense) characters.
    overwrite : `bool`, optional, default: `False`
        Overwrite any existing index files. The default behaviour is to raise a
        ``FileExistsError`` if an existing index file is found in the same
        directory as the output file.
    verbose : `bool`, optional, default: `False`
        Report index building process.
    index_length : `int`, optional, default: `NoneType`
        A positive integer for the length of the index values (in character).
        This is also applied to integer values. If it is ``NoneType`` then the
        value with the maximum length in the index column is used for index
        length.

    Returns
    -------
    tempidx : `str`
        The path to the temporary index file that will eventually be loaded
        into the index database.
    index_file : `str`
        The path to the final index file. This is only the path and will not
        contain the index file at this stage.
    info : `sqlalchemy_config.orm_code.ColumnInfo`
        The column info file specifying the column data types and character
        lengths.

    Raises
    ------
    IOError
        If the input file is compressed but not with bgzip.
    """
    tqdm_kwargs = dict(
        unit=" rows", desc="[info] indexing input", disable=not verbose
    )

    # Will eventually hold the file object for provisional writing of the
    # index file
    idxfobj = None
    try:
        is_bgzipped = utils.is_bgzip(infile)

        # If the input file is compressed then it must be compressed with BGZIP
        # So we can test for this
        if is_bgzipped is False and \
           (True in [utils.is_gzip, utils.is_bzip2]):
            raise IOError(
                "indexing input file must be uncompressed or compressed with"
                " bgzip, can not be gzip/bzip2 compressed"
            )

        # Get the method to use to open the input file.
        # Get the method to use to open the input file, this will raise a
        # TypeError if the open method can't be worked out.
        inopen_method = common.get_open_method(infile)
        # TODO: deal with the encoding issue
        with inopen_method(infile, mode='rt') as incsv:
            # Open the input file and get the header row whilst respecting
            # the number of skip lines and comment characters.
            # iter(f.readline, '')
            # https://stackoverflow.com/questions/29618936
            reader = csv.reader(
                iter(incsv.readline, ''), delimiter=delimiter
            )

            # TODO: check index column is unique in the header
            header_row, rowno = header.get_header_from_reader(
                reader, skiplines=skiplines, comment=comment
            )

            # This will be the seek position of the first data row (unless is
            # a comment row)
            pos = incsv.tell()

            # Find the index column in the header row
            col_idx = _get_index_col(header_row, index_col)

            # Use the default index file name or generate one
            index_file = index_file or _get_index_file(
                header_row[col_idx], infile
            )

            # Sanity check to prevent any accidental overwrites
            if index_file == infile:
                raise ValueError(
                    "index file name is the same as input name"
                )

            # Make sure the index does not exist or it it does and we want
            # to overwrite then we clear it
            _check_index_file(index_file, overwrite)

            # Create a temp index file, open and write the header
            tempidx = os.path.join(tmpdir, os.path.basename(index_file))
            idxfobj = gzip.open(tempidx, 'wt')
            idx_writer = csv.writer(idxfobj, delimiter="\t")
            idx_writer.writerow(database.INDEX_HEADER)

            # This will be used to determine the data types of the columns
            # in the file being indexed
            info = ColumnInfo(header_row)

            # Make sure the index column is flagged as True
            info.columns[col_idx].index = True

            # Get the first data row in the file being indexed
            first_row, pos = _get_first_row(incsv, reader, comment)
            info.add_row(first_row)

            # Get the index value accounting for the index length a NoneType
            # index_length will return the whole string
            prev_idx = first_row[col_idx][:index_length]

            # When consecutive lines have the same index value we will only
            # store the first one in the index along with the number of
            # additional lines that need to be read in. This means that files
            # sorted on the index column can generate smaller indexes.
            line_run = 1
            # The position after the first row has been read in, so the start
            # of the second row
            prev_pos = incsv.tell()
            row_no = 1
            for row in tqdm(reader, initial=1, **tqdm_kwargs):
                if comment is not None and row[0].startswith(comment):
                    line_run += 1
                    pos = incsv.tell()
                    continue

                info.add_row(row)
                idx = row[col_idx][:index_length]

                # If the current index value is different from the previous one
                # then write the previous one to the index file and set up
                # for the next one
                if idx != prev_idx:
                    # TODO: Check unicode
                    idx_writer.writerow(
                        [row_no, prev_idx, pos, prev_pos, line_run]
                    )
                    pos = prev_pos
                    line_run = 0
                prev_pos = incsv.tell()
                prev_idx = idx
                line_run += 1
                row_no += 1
            # Final write
            idx_writer.writerow([row_no, prev_idx, pos, prev_pos, line_run])
    except TypeError:
        # From the input open method detection
        if idxfobj is not None:
            idxfobj.close()
        raise NotImplementedError("can't use file objects yet")
    except Exception:
        if idxfobj is not None:
            idxfobj.close()
        raise

    # # Everything is now setup to create the SQLite database for the index
    # # So close all the files
    idxfobj.close()
    return tempidx, index_file, info


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_index_info(column_info):
    """Get the column index column from a bunch of column info objects.

    Parameters
    ----------
    column_info : `sqlalchemy_config.orm_code.ColumnInfo`
        The column info data.

    Returns
    -------
    column_idx : `sqlalchemy_config.orm_code.FileColumn`
        The column representation of the index column.

    Raises
    ------
    ValueError
        If the index column can't be found or there is > one index column.
    """
    # Identify the index column in the column definitions. There must be a
    # single column defined
    index_col = [i for i in column_info.columns if i.index is True]
    if len(index_col) != 1:
        raise ValueError(
            "expecting a single index column defined: {len(index_col)}"
        )
    return index_col[0]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_index_col(header_row, index_col):
    """Get the column number of the index column in the header of the file
    being indexed.

    Parameters
    ----------
    header_row : `list` of `str`
        The header columns.
    index_col : `str`
        The index column name.

    Returns
    -------
    column_idx : `int`
        The 0-based column number for the index column in the header.

    Raises
    ------
    ValueError
        If the index column can't be found in the file header row or if there
        are repeated column names in the header row.
    """
    try:
        col_idx = header_row.index(index_col)
    except ValueError as e:
        raise ValueError(
            f"can't find index column name in the header: {index_col}"
        ) from e

    dup_cols = [i for i in header_row[col_idx+1:] if i == index_col]
    if len(dup_cols) > 0:
        raise ValueError(
            f"repeated index column name in the header: {len(dup_cols)+1}"
        )

    return col_idx


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_index_file(index_col, indexed_file):
    """Generate an index column name from the index column name and the name
    of the file being indexed.

    Parameters
    ----------
    index_col : `str`
        The name of the column being indexed.
    indexed_file : `str`
        The name of the file being indexed.

    Returns
    -------
    idx_file : `str`
        The name of the final index file.
    """
    # Get the full path to the file being indexed
    indexed_file = os.path.realpath(os.path.expanduser(indexed_file))
    indexed_file = Path(indexed_file)

    # Get all the extensions from the file being indexed
    extensions = "".join(indexed_file.suffixes)

    # Remove any extensions from the file being indexed to create a based
    # index file name
    indexed_file_base = str(indexed_file).replace(extensions, "")

    # Add a processed column name to the base index file name to create a
    # final path to the index file
    idx_file_base = "{0}.idx".format(
        re.sub(r'\W+', '_', index_col.lower())
    )
    idx_file = f"{indexed_file_base}-{idx_file_base}"

    return idx_file


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_first_row(indexed_file_obj, indexed_file_reader, comment):
    """Get the first data row from the file being indexed.

    Parameters
    ----------
    indexed_file_obj : `File`
        The file object for the file being indexed.
    indexed_file_reader : `csv.Reader`
        The csv reader to provide rows from the input file (might not be the
        same as the one being indexed).
    comment : `str`
        The comment character for rows to skip.

    Returns
    -------
    first_row : `list` of `str`
        The first non comment row.
    pos : `int`
        The seek position before the first non-comment row in the file being
        indexed.
    """
    if comment is None:
        pos = indexed_file_obj.tell()
        row = next(indexed_file_reader)
        return row, pos

    row = [comment]

    while row[0].startswith(comment):
        pos = indexed_file_obj.tell()
        try:
            row = next(indexed_file_reader)
        except StopIteration as e:
            raise StopIteration("no data in file being indexed") from e
    return row, pos


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _check_index_file(index_file, overwrite):
    """Check to see if the index file exists. If it does and overwrite is
    ``False`` then raise a ``FileExistsError``.

    Parameters
    ----------
    index_file : `str`
        The path to a final index file.
    overwrite : `bool`
        If the index file exists do you want to overwrite it.

    Rasies
    ------
    FileExistsError
        If the index file exists and ``overwrite`` is ``False``.
    """
    try:
        open(index_file).close()
    except FileNotFoundError:
        return

    # If we get here then it exists
    if overwrite is False:
        raise FileExistsError(f"index file already exists: {index_file}")

    # If we get here then we want to overwrite, so we zap it to stop SQLite
    # adding to the existing database
    os.unlink(index_file)
    # open(index_file, 'wb').close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _sort_index(index_file, info, tmpdir, verbose=False):
    """Sort (chunk) the index file by the index column.

    Parameters
    ----------
    index_file : `str`
        The path to the index file.
    info : `sqlalchemy_config.orm_code.ColumnInfo`
        The info object holding the data on each column in the file being
        indexed.
    tmpdir : `str`
        The temp directory to use for the sort.
    verbose : `bool`, optional, default: `NoneType`
        Report progress when reading through the index file.

    Returns
    -------
    chunk_files : `list` of `str`
        The paths to chunk files that contain batches of sorted rows from the
        index file.
    """
    with gzip.open(index_file, 'rt', encoding=_INDEX_ENCODING) as infile:
        reader = csv.reader(infile, delimiter=_INDEX_DELIMITER)
        header_row = next(reader)
        indexed_col = _get_index_info(info)

        dtype = _get_sort_func(indexed_col)

        # Find the index column in the header row
        col_idx = _get_index_col(header_row, database.IDX_VAL_COL_NAME)

        tqdm_kwargs = dict(
            disable=not verbose, total=info.nrows, unit=" row(s)",
            desc=f'[info] sorting index on column {col_idx} as'
            f' {indexed_col.dtype}', leave=False
        )
        with chunks.CsvSortedChunks(
                tmpdir, key=lambda x: dtype(x[col_idx]),
                header=header_row, chunk_prefix="index_sort",
                encoding=_INDEX_ENCODING, delimiter=_INDEX_DELIMITER
        ) as chunker:
            for row in tqdm(reader, **tqdm_kwargs):
                chunker.add_row(row)
        return chunker.chunk_file_list


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _merge_generator(chunk_files, info, tmpdir):
    """Yield sorted rows from the index file chunks.

    Parameters
    ----------
    chunk_files : `list` of `str`
        The paths to chunk files that contain batches of sorted rows from the
        index file.
    info : `sqlalchemy_config.orm_code.ColumnInfo`
        The info object holding the data on each column in the file being
        indexed.
    tmpdir : `str`
        The temp directory to use for the sort.

    Yields
    ------
    row : `list` of `str`
        The rows from the index file, all the columns are strings at this
        stage. The first row yielded is the header row.
    """
    with gzip.open(chunk_files[0], 'rt', encoding=_INDEX_ENCODING) as test:
        reader = csv.reader(test, delimiter=_INDEX_DELIMITER)
        header_row, skiplines = header.get_header_from_reader(reader)

    # Get the indexed column
    indexed_col = _get_index_info(info)

    dtype = _get_sort_func(indexed_col)

    # Find the index column in the header row
    col_idx = _get_index_col(header_row, database.IDX_VAL_COL_NAME)

    with merge.CsvIterativeHeapqMerge(
            chunk_files, tmpdir=tmpdir, key=lambda x: dtype(x[col_idx]),
            header=True, read_kwargs=dict(encoding=_INDEX_ENCODING),
            write_kwargs=dict(encoding=_INDEX_ENCODING),
            csv_kwargs=dict(delimiter=_INDEX_DELIMITER),
            delete=True
    ) as merger:
        yield merger.header

        for row in merger:
            yield row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _index_generator(index_file):
    """Yield index file rows.

    Parameters
    ----------
    index_file : `str`
        The index file path.

    Yields
    ------
    row : `list` of `str`
        The rows from the index file, all the columns are strings at this
        stage. The first row yielded is the header row.
    """
    with gzip.open(index_file, 'rt', encoding=_INDEX_ENCODING) as infile:
        reader = csv.reader(infile, delimiter=_INDEX_DELIMITER)
        header_row, skiplines = header.get_header_from_reader(reader)
        yield header_row

        for row in reader:
            yield row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_index_db(db_name, indexed_file, index_file, col_info, index_length,
                    skiplines, comment, delimiter, commit_every=10000,
                    verbose=False, build_class=database.DuckDbIndex,
                    tmpdir=None):
    """Build an index database file.

    Parameters
    ----------
    db_name : `str`
        The path to the database.
    indexed_file : `str`
        The file path for the file being indexed.
    index_file : `str`
        The file path for the temp index file with the data to write to the
        index database.
    col_info : `sqlalchemy_config.orm_code.ColumnInfo`
        The data types for the columns in the ``indexed_file``, one of these
        must be flagged with ``index=True``.
    index_length : `int` or `NoneType`
        The length of the index. NoneType will default to the max length
        value in the indexed column.
    skiplines : `int`
        The number of lines that are needed to be skipped before reading the
        ``indexed_file`` header. These are stored in the metadata table.
    comment : `str`
        The comment character(s). Lines starting with comment characters are
        skipped. This is stored in the metadata table.
    delimiter : `str`
        The delimiter of the file being indexed, this will be stored in the
        index database metadata.
    commit_every : `int`, optional, default: `10000`
        Buffer ``commit_every`` rows in memory before loading and committing to
        the database.
    verbose : `bool`, optional, default: `False`
        Report index creation progress.
    build_class : `class`, optional, default: `database.DuckDbIndex`
        The index build class to use.
    tmpdir : `str`, optional, default: `NoneType`
        The temporary directory to use. If not supplied then the system temp
        location is used.

    Returns
    -------
    index_db_path : `str`
        A temp path to the index database.

    Raises
    ------
    FileExistsError
        If the database already exists.
    ValueError
        If a single index column is not defined in ``col_info``.

    Notes
    -----
    The index database file should not exist.
    """
    db_file = f"{db_name}.db"

    try:
        open(db_file).close()
        raise FileExistsError(
            "temp database file already exists, this should not happen and"
            f" is probably a bug: {db_file}"
        )
    except FileNotFoundError:
        pass

    # header_row = header.get_header_from_file(chunk_files[0])

    # Get the indexed column
    index_col = _get_index_info(col_info)
    index_length = index_length or index_col.max_len

    # Get the file size of the indexed file to store in the index metadata
    file_size = os.path.getsize(indexed_file)
    # print("FILE SIZE DB")
    # print(file_size)
    with build_class(db_file, read_only=False, tmpdir=tmpdir) as db:
        db.create_tables(index_col, index_length=index_length)
        file_rows, index_rows = db.import_index_file(
            index_file, index_col, commit_every=commit_every, verbose=verbose
        )
        db.import_indexed_metadata(
            file_size, file_rows, index_rows, skiplines, comment,
            index_length, delimiter
        )
        db.import_column_definitions(col_info)
        # pp.pprint(db.get_metadata())
    return db_file


def _get_sort_func(indexed_col):
    parse_func = indexed_col.Dtype.get_python_cast(
        indexed_col.dtype, allow_none=indexed_col.nullable
    )

    if indexed_col.dtype in [
            indexed_col.Dtype.DATE,
            indexed_col.Dtype.DATE_TIME
    ]:
        def _sort_date_func(x):
            x = parse_func(x)
            if x is None:
                return datetime.datetime(datetime.MAXYEAR, 12, 31)
            return x
        return _sort_date_func

    elif indexed_col.dtype in [
            indexed_col.Dtype.INTEGER,
            indexed_col.Dtype.FLOAT,
            indexed_col.Dtype.BOOLEAN
    ]:
        def _sort_func_numeric(x):
            x = parse_func(x)
            if x is None:
                return math.inf
            return x
        return _sort_func_numeric
    return parse_func
