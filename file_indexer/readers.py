"""Lower level API classes for querying the indexed files.
"""
from file_indexer import (
    common,
    database,
    errors
)
from pyaddons.flat_files import header
from sqlalchemy_config import utils
from sqlalchemy_config.utils import file_db_type
from sqlalchemy_config.orm_code import FileColumn
import pandas as pd
import csv
import os
# import pprint as pp

DB_CLASS = {
    utils.DUCKDB_TYPE: database.DuckDbIndex,
    utils.SQLITE_TYPE: database.SqliteIndex
}
"""A mapping between the database type string and database index classes
(`dict`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class IndexReader(object):
    """Query the index and extract data from the indexed file. The data is
    returned as strings.

    Parameters
    ----------
    indexed_file : `str`
        The path to the indexed file that is being queried via the index.
    index : `str`, optional, default: `NoneType`
        An index files to search in. If they are not provided then
        it is located based on the indexed file name.
    column : `str`, optional, default: `NoneType`
        A specific column name you want to query. This can be used instead of
        the index argument if the file index is the same basename as the data
        file.
    return_type : `type` of (`list` or `dict`)
        The return type of the queried rows. This must be a `dict` or `list`
        class (not object).

    Raises
    ------
    NotImplementedError
        If multiple index files are detected.
    FileNotFoundError
        If no index files are detected.
    IOError
        If no indexes can be auto-detected. If the indexed file is gzipped but
        not bgzipped.
    """
    ROW_IDX_COL_IDX = database.ROW_IDX_COL_IDX
    """The column index for the row number column in the index file and DB
    (`int`)
    """
    IDX_VAL_COL_IDX = database.IDX_VAL_COL_NAME
    """The column index for the index value in the index file and DB (`int`)
    """
    SEEK_COL_IDX = database.SEEK_COL_IDX
    """The column index for the seek position value in the index file and DB
    (`int`)
    """
    SEEK_END_COL_IDX = database.SEEK_END_COL_IDX
    """The column index for the seek end position value in the index file and DB
    (`int`)
    """
    LINE_RUN_COL_IDX = database.LINE_RUN_COL_IDX
    """The column index for the line run value in the index file and DB (`int`)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, indexed_file, index=None, column=None,
                 return_type=list, typed=False):
        self._indexed_file_path = indexed_file
        self._index_obj = None
        self._metadata = None
        self._index_col = None
        self._header = None
        self._file = None
        self._reader = None

        if index is not None:
            self._index_path = index
        elif column is not None:
            self._index_path = common.get_indexes(
                self._indexed_file_path, column=column
            )[0]
        else:
            self._index_path = common.get_indexes(self._indexed_file_path)
            if len(self._index_path) > 1:
                raise NotImplementedError(
                    "multiple index queries not supported yet, please supply a"
                    " single index file name if you have multiple indexes"
                )

            if len(self._index_path) == 0:
                raise FileNotFoundError("No index files found")

            self._index_path = self._index_path[0]

        # Get the open method of the index file.
        self._open_method = common.get_open_method(self._indexed_file_path)
        self._return_type = return_type
        self._columns = []
        self._typed = False
        self.typed = typed

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """Open the index reader and all the DB connections/indexed file.
        """
        # Initialise the index database
        self._index_obj = DB_CLASS[file_db_type(self._index_path)](
            self._index_path
        )
        # print("INDEX PATH")
        # print(self._index_path)
        self._index_obj.open()

        # Now validate the index, this makes sure that the header is the
        # expected length and the index file size has not changed
        self._metadata, self._columns, self._index_col, self._header, \
            self._file, self._reader = self._validate_index(self._index_obj)

        self.typed = self._typed
        # if self._typed is True:
        #     self._type_casts = []
        #     for i in self._columns:
        #         self._type_casts.append(
        #             FileColumn.Dtype.get_python_cast(
        #                 i['data_type'], allow_none=i['has_null']
        #             )
        #         )
        #     if self._return_type == list:
        #         self._type_func = self._type_list
        #     elif self._return_type == dict:
        #         self._type_func = self._type_dict
        # else:
        #     self._type_func = self._dummy

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def typed(self):
        return self._typed

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @typed.setter
    def typed(self, typed):
        self._typed = typed
        self._set_typed()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _set_typed(self):
        if self._typed is True:
            self._type_casts = []
            for i in self._columns:
                self._type_casts.append(
                    FileColumn.Dtype.get_python_cast(
                        i['data_type'], allow_none=i['has_null']
                    )
                )
            if self._return_type == list:
                self._type_func = self._type_list
            elif self._return_type == dict:
                self._type_func = self._type_dict
        else:
            self._type_func = self._dummy

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Close the index reader and all the DB connections/open files.
        """
        try:
            # Close the index database
            self._index_obj.close()
        except AttributeError:
            # Not open
            pass

        try:
            # Close the actual data file
            self._file.close()
        except AttributeError:
            # Not open
            pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _validate_index(self, index_obj):
        """Validate an index the indexed data file.

        Parameters
        ----------
        index_obj : (`file_indexer.database.SqliteIndex` or \
        `file_indexer.database.DuckDbIndex`)
            The index being validated.

        Returns
        -------
        metadata : `dict`
            The index metadata.
        columns : `list` of `dict`
            The column information in the indexed data file.
        index_col : `dict`
            The column information for the indexed column.
        header_row : `list` of `str`
            The header row from the indexed data file.
        incsv : `File`
            The indexed data file object.
        reader : `csv.reader` or `csv.DictReader`
            The CSV reader object attached to the indexed data file.

        Raises
        ------
        file_indexer.errors.BadIndexError
            If the indexed data file does not have the expected file size. If
            > 1 index column is defined in the index. If the index data file
            does not have the expected header columns.
        TypeError
            If the return type is not `list` of `dict` type classes.
        """
        # Get all the metadata associated with the index
        metadata = index_obj.get_metadata()
        # pp.pprint(metadata)
        # First ensure the file size has not changed
        if metadata[index_obj.META_FILE_SIZE] != \
           os.path.getsize(self._indexed_file_path):
            raise errors.BadIndexError(
                "wrong index file size: expected "
                f"{metadata[index_obj.META_FILE_SIZE]} "
                f"found {os.path.getsize(self._indexed_file_path)}"
            )

        # Get all the column information for the input file.
        columns = index_obj.get_columns()

        # Extract the indexed column information and make sure it is
        # unique
        index_col = [i for i in columns if i[index_obj.COL_IS_INDEX] is True]
        if len(index_col) != 1:
            raise errors.BadIndexError(
                "multiple index columns defined in index metadata: "
                f"{len(index_col)} "
            )
        index_col = index_col[0]

        # Now compare the header row to what is expected in the index
        exp_header = [i[index_obj.COL_NAME] for i in columns]

        csv_kwargs = dict(delimiter=metadata[index_obj.META_DELIMITER])

        # Now open the data file
        incsv = self._open_method(self._indexed_file_path, 'rt')

        try:
            # Extract the header and check it
            header_row, rowno = header.get_header_from_file(
                incsv, skiplines=metadata[index_obj.META_SKIP_LINES],
                comment=metadata[index_obj.META_COMMENT]
            )

            # The first row after the header has been extracted
            self.data_row_seek = incsv.tell()

            header_row = next(csv.reader([header_row], **csv_kwargs))

            if exp_header != header_row:
                ex = ",".join(exp_header)
                got = ",".join(header_row)
                raise errors.BadIndexError(
                    f"bad index file header: expected {ex}, got {got}"
                )

            # Assign the data file to the correct reader depending on the
            # return type
            reader = None
            if self._return_type == list:
                reader = csv.reader(incsv, **csv_kwargs)
            elif self._return_type == dict:
                reader = csv.DictReader(incsv, header_row, **csv_kwargs)
            else:
                raise TypeError(f"unknown return type: {self._return_type}")
        except Exception:
            # Tidy up if something has gone wrong
            incsv.close()
            raise
        return metadata, columns, index_col, header_row, incsv, reader

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def seek(self, to):
        """Expose the underlying file objects seek method.

        Parameters
        ----------
        to : `int`
            The position in the indexed file to seek to.
        """
        self._file.seek(to)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def header(self):
        """ Return the header of the indexed file (`list` of `str`)
        """
        return self._header

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def file_obj(self):
        """Get the file object (`file`).
        """
        return self._file

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def csv_reader(self):
        """Get the CSV reader object (`csv.reader` or `csv.DictReader`).
        """
        return self._reader

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def metadata(self, ignore_type=False):
        """Query the metadata from the index.

        Returns
        -------
        metadata : `list` or `dict`
            A single un-formatted row
        """
        md = self._index_obj.get_metadata()

        if self._return_type == list and ignore_type is False:
            return list(md.values())
        return md

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def columns(self):
        """Get the column information for the index.

        Returns
        -------
        columns : `list` of (`list` or `dict`)
            The column information that has been derived from the data file.
        """
        cols = self._index_obj.get_columns()
        if self._return_type == list:
            return [i.values() for i in cols]
        return cols

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def query(self, values, file_order=False):
        """Query for one or more values, each value is applied in an OR
        construct.

        Parameters
        ----------
        values : `list` of `tuples`
            One or more things to query. Each tuple is a query operator at 0
            and the query value at 1. Valid query operators are: ``=``, ``==``,
            ``=~`` (regex), ``>=``, ``<=``, ``>``, ``<``, ``LIKE``, ``BETWEEN``
            . The regexp operator can have an additional flag at position 2 in
            the tuple indicating any regex flags, such as ``i`` for case
            insensitive. Please note the regex is not valid for SQLite indexes
            and is mapped to a LIKE operator. Additionally, a ``WHERE``
            operator can be given. In this case, the query value at one is
            expected to be a formatted where clause for an SQL query, relevant
            for the index database type.

        Yields
        ------
        row : (`list` or `dict`) of `str`
            A single un-formatted row

        Raises
        ------
        IndexError
            If the returned row length is not the same as the header length.
        """
        exp_len = len(self.header)

        for row in self._index_obj.query_index(values, file_order=file_order):
            self._file.seek(row[self.SEEK_COL_IDX])

            for i in range(row[self.LINE_RUN_COL_IDX]):
                row = next(self._reader)
                if len(row) != exp_len:
                    raise IndexError(
                        "row ('{0}') is not the same length as"
                        " header ('{1}') ".format(
                            len(row), exp_len
                        )
                    )
                yield self._type_func(row)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _dummy(self, row):
        return row

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _type_list(self, row):
        return [f(v) for f, v in zip(self._type_casts, row)]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _type_dict(self, row):
        return dict(
            [
                (k, f(v)) for f, k, v in zip(
                    self._type_casts, row.keys(), row.values()
                )
            ]
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def total_count(self, values):
        """Query for one or more values and get the total number of rows that
        would be returned.

        Parameters
        ----------
        values : `list` of `tuples`
            One or more things to query. Each tuple is a query operator at 0
            and the query value at 1. Valid query operators are: ``=``, ``==``,
            ``=~`` (regex), ``>=``, ``<=``, ``>``, ``<``, ``LIKE``, ``BETWEEN``
            . The regexp operator can have an additional flag at position 2 in
            the tuple indicating any regex flags, such as ``i`` for case
            insensitive. Please note the regex is not valid for SQLite indexes
            and is mapped to a LIKE operator. Additionally, a ``WHERE``
            operator can be given. In this case, the query value at one is
            expected to be a formatted where clause for an SQL query, relevant
            for the index database type.

        Returns
        -------
        total_rows : `int`
            The total number of rows that would be returned from an actual data
            query.
        """
        total_count = 0
        for row in self._index_obj.query_index(values):
            total_count += row[self.LINE_RUN_COL_IDX]
        return total_count

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def value_count(self, values, disk=False):
        """Query for one or more values and get the total number of rows for
        each unique index value. This is a similar to a group by and count.

        Parameters
        ----------
        values : `list` of `tuples`
            One or more things to query. Each tuple is a query operator at 0
            and the query value at 1. Valid query operators are: ``=``, ``==``,
            ``=~`` (regex), ``>=``, ``<=``, ``>``, ``<``, ``LIKE``, ``BETWEEN``
            . The regexp operator can have an additional flag at position 2 in
            the tuple indicating any regex flags, such as ``i`` for case
            insensitive. Please note the regex is not valid for SQLite indexes
            and is mapped to a LIKE operator. Additionally, a ``WHERE``
            operator can be given. In this case, the query value at one is
            expected to be a formatted where clause for an SQL query, relevant
            for the index database type.
        disk : `bool`, optional, default: `False`
            Use on disk caching of counts instead of in memory caching. This is
            not implemented yet.

        Yields
        ------
        row : (`list` or `dict`) of `str`
            Each row is a unique index value and a count of the number of rows
            that match to it.

        Notes
        -----
        Please note that this will perform all the counts in memory, so if your
        index is large and your query returns lots of rows then memory
        consumption may be high.
        """
        if disk is True:
            raise NotImplementedError("on disk caching is not ready yet!")

        value_count = dict()
        for row in self._index_obj.query_index(values):
            try:
                value_count[row[1]] += row[self.LINE_RUN_COL_IDX]
            except KeyError:
                value_count[row[1]] = row[self.LINE_RUN_COL_IDX]

        if self._return_type == list:
            for k, v in value_count.items():
                yield [k, v]
        else:
            for k, v in value_count.items():
                yield dict(index_value=k, count=v)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PandasIndexReader(IndexReader):
    """Query the index and extract data from the indexed file. The data is
    returned as a pandas data frame.

    Parameters
    ----------
    indexed_file : `str`
        The path to the indexed file that is being queried via the index.
    index : `str`, optional, default: `NoneType`
        An index files to search in. If they are not provided then
        it is located based on the indexed file name.
    column : `str`, optional, default: `NoneType`
        A specific column name you want to query. This can be used instead of
        the index argument if the file index is the same basename as the data
        file.

    Raises
    ------
    NotImplementedError
        If multiple index files are detected.
    FileNotFoundError
        If no index files are detected.
    IOError
        If no indexes can be auto-detected. If the indexed file is gzipped but
        not bgzipped.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, indexed_file, index=None, column=None, typed=True):
        super().__init__(indexed_file, index=index, column=column,
                         return_type=dict, typed=typed)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def metadata(self):
        """Query the metadata from the index.

        Returns
        -------
        metadata : `pandas.DataFrame`
            A dataframe with a single metadata row.
        """
        md = super().metadata()
        return pd.DataFrame([list(md.values())], columns=md.keys())

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def columns(self):
        """Get the column information for the index.

        Returns
        -------
        columns : `pandas.DataFrame`
            A dataframe with the column information in each row.
        """
        return pd.DataFrame(super().columns())

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def query(self, values, **kwargs):
        """Query for one or more values, each value is applied in an OR
        construct.

        Parameters
        ----------
        values : `list` of `tuples`
            One or more things to query. Each tuple is a query operator at 0
            and the query value at 1. Valid query operators are: ``=``, ``==``,
            ``=~`` (regex), ``>=``, ``<=``, ``>``, ``<``, ``LIKE``, ``BETWEEN``
            . The regexp operator can have an additional flag at position 2 in
            the tuple indicating any regex flags, such as ``i`` for case
            insensitive. Please note the regex is not valid for SQLite indexes
            and is mapped to a LIKE operator. Additionally, a ``WHERE``
            operator can be given. In this case, the query value at one is
            expected to be a formatted where clause for an SQL query, relevant
            for the index database type.

        Returns
        -------
        data : pandas.DataFrame
            A data frame of results.

        Raises
        ------
        IndexError
            If the returned row length is not the same as the header length.
        """
        # return pd.DataFrame(self._query(values, **kwargs))
        return pd.DataFrame(
            self._query(values, **kwargs), columns=self._header
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def value_count(self, values, **kwargs):
        """Query for one or more values and get the total number of rows for
        each unique index value. This is a similar to a group by and count.

        Parameters
        ----------
        values : `list` of `tuples`
            One or more things to query. Each tuple is a query operator at 0
            and the query value at 1. Valid query operators are: ``=``, ``==``,
            ``=~`` (regex), ``>=``, ``<=``, ``>``, ``<``, ``LIKE``, ``BETWEEN``
            . The regexp operator can have an additional flag at position 2 in
            the tuple indicating any regex flags, such as ``i`` for case
            insensitive. Please note the regex is not valid for SQLite indexes
            and is mapped to a LIKE operator. Additionally, a ``WHERE``
            operator can be given. In this case, the query value at one is
            expected to be a formatted where clause for an SQL query, relevant
            for the index database type.
        **kwargs
            Ignored, note in disk caching is not used for data frames.

        Returns
        -------
        data : pandas.DataFrame
            A data frame of results.
        """
        return pd.DataFrame(super().value_count(values))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _query(self, values, **kwargs):
        """Perform a data query and process the results row into the correct
        data type

        Parameters
        ----------
        values : `list` of `tuples`
            One or more things to query. Each tuple is a query operator at 0
            and the query value at 1. Valid query operators are: ``=``, ``==``,
            ``=~`` (regex), ``>=``, ``<=``, ``>``, ``<``, ``LIKE``, ``BETWEEN``
            . The regexp operator can have an additional flag at position 2 in
            the tuple indicating any regex flags, such as ``i`` for case
            insensitive. Please note the regex is not valid for SQLite indexes
            and is mapped to a LIKE operator. Additionally, a ``WHERE``
            operator can be given. In this case, the query value at one is
            expected to be a formatted where clause for an SQL query, relevant
            for the index database type.

        Yields
        ------
        row : `dict`
            A results row with the keys being column names and the values
            being correctly typed data values.
        """
        for row in super().query(values, **kwargs):
            yield row
