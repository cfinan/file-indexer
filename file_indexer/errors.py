"""Some custom errors.
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BadIndexError(Exception):
    """An error that is raised when a bad index file is found.

    Parameters
    ----------
    message : `str`
        The error message.
    """
    pass
