"""Query an indexed file.
"""
from file_indexer import (
    __version__,
    __name__ as pkg_name,
    common,
    readers
)
from sqlalchemy_config.utils import file_db_type
from sqlalchemy_config.orm_code import FileColumn
from pyaddons import log, utils
import stdopen
import argparse
import sys
import os
import csv
import re
# import pprint as pp


_SCRIPT_NAME = "fidx-query"
"""The name of the script (`str`)
"""
_DESC = __doc__
"""The program description (`str`)
"""
# OPERATOR_REGEX = re.compile(
#     r'==|(?P<FLAGS>[cilmnps])?=~|[><]=?|WHERE|LIKE|BETWEEN|=',
#     re.IGNORECASE
# )
OPERATOR_REGEX = re.compile(
    r'==|(?P<FLAGS>[cilmnps])?=~|[><]=?|WHERE|LIKE|BETWEEN|=',
    re.IGNORECASE
)
"""A regular expression to match operators in query strings (`re.Pattern`)
"""
DATA_QUERY = 'data'
"""A keyword representing a data query (`str`)
"""
TOTAL_COUNT_QUERY = 'tcount'
"""A keyword representing a total count query (`str`)
"""
VALUE_COUNT_QUERY = 'vcount'
"""A keyword representing a value count query (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    # Do we want to turn on progress monitoring
    prog_verbose = log.progress_verbose(verbose=args.verbose)

    indexes = [args.index_file]
    if args.index_file is None:
        indexes = common.get_indexes(args.infile)

    for i in indexes:
        dbtype = file_db_type(i)
        logger.info(f"found index: {i} ({dbtype})")

    if len(indexes) > 1:
        raise NotImplementedError(
            "multiple index queries not supported yet, please supply a"
            " single index file name if you have multiple indexes"
        )

    # Only want a single index
    index = indexes[0]

    # Get the open method for the output file. If it has a .gz extension
    # it will be gzip, otherwise with open
    open_method = utils.get_open_method(args.outfile, allow_none=True)
    try:
        # If output file is not defined then output will be to stdout
        # otherwise, file output is via tempfile
        with stdopen.open(
                args.outfile, mode='wt', method=open_method, use_tmp=True,
                tmpdir=args.tmpdir
        ) as outfh:
            writer = csv.writer(outfh, delimiter=args.delimiter,
                                lineterminator=os.linesep)
            if args.type == DATA_QUERY:
                # Perform a data query
                for row in query_data(
                        args.infile, args.queries, index=index,
                        verbose=prog_verbose
                ):
                    writer.writerow(row)
            if args.type == VALUE_COUNT_QUERY:
                # Perform a value count query
                for row in query_value_count(
                        args.infile, args.queries, index=index,
                        verbose=prog_verbose
                ):
                    writer.writerow(row)
            elif args.type == TOTAL_COUNT_QUERY:
                # Perform a total count query
                total_count = query_total_count(
                    args.infile, args.queries, index=index,
                    verbose=prog_verbose
                )
                writer.writerow([total_count])
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        log.log_end(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        'infile', type=str, default='-',
        help="The path to the indexed file"
    )
    parser.add_argument(
        'queries', type=str, nargs='+',
        help="One or more queries to run against the index. If a string is "
        "provided this will be an euqals query. Regexes can be used with "
        "'=~<QUERY>'. See `here <https://cfinan.gitlab.io/file-indexer/api/"
        "file_indexer_package.html#file_indexer.readers.IndexReader.query>`_"
        " for explainations of other operators."
    )
    parser.add_argument(
        '--type', choices=[DATA_QUERY, TOTAL_COUNT_QUERY, VALUE_COUNT_QUERY],
        type=str, default=DATA_QUERY,
        help="The query type, either a full data query, total count of rows "
        "or counts of each value."
    )
    parser.add_argument(
        '-o', '--outfile',
        type=str,
        help="Write the query output to an output file. If not provided "
        "then query is output to STDOUT. If the output file has a .gz "
        "extension it will be compressed."
    )
    parser.add_argument(
        '-T', '--tmpdir',
        type=str,
        help="File output is written via temp, you can change the temp"
        " directory here. The default is to use the system tmp location."
    )
    parser.add_argument(
        '-d', '--delimiter', type=str, default="\t",
        help="The delimiter of the output file"
    )
    parser.add_argument(
        '--index-file', type=str,
        help="An alternative name for the index file. The default is to use"
        " the name of the file being indexed joined to the indexing column"
        " name with a hyphen. The indexing column name is preprocessed to"
        " make lowercase, remove spaces and non-word characters."
    )
    parser.add_argument(
        '-v', '--verbose',  action="count",
        help="Give more output, use -vv to turn on progress monitoring"
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments.
    """
    args = parser.parse_args()
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def query_total_count(indexed_file, query_terms, index=None, verbose=False):
    """Query an ``indexed_file`` using the ``indexes`` and ``query_terms`` for
    the total number of matching rows.

    Parameters
    ----------
    indexed_file : `str`
        The path to the file that is being queried.
    query_terms : `list` of `str`
        The query terms passed by the user. These will be processed into
        queries appropriate for the reader object.
    index : `str`, optional, default: `NoneType`
        The index file to use, if not supplied then it will be detected.
    verbose : `bool`, optional, default: `False`
        Not used at present.

    Returns
    -------
    total_count : `int`
        The total number of rows that the query will return.
    """
    query_terms = _process_query_terms(query_terms)
    kwargs = dict(index=index, return_type=list)
    with readers.IndexReader(indexed_file, **kwargs) as q:
        return q.total_count(query_terms)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def query_value_count(indexed_file, query_terms, index=None, verbose=False):
    """Query an ``indexed_file`` using the ``indexes`` and ``query_terms`` for
    counts of number of rows for each index value match.

    Parameters
    ----------
    indexed_file : `str`
        The path to the file that is being queried.
    query_terms : `list` of `str`
        The query terms passed by the user. These will be processed into
        queries appropriate for the reader object.
    index : `str`, optional, default: `NoneType`
        The index file to use, if not supplied then it will be detected.
    verbose : `bool`, optional, default: `False`
        Not used at present.

    Yields
    ------
    row : `list`
        Each row is an index value and the total number of rows associated
        with it. The first row yielded will be the header.
    """
    query_terms = _process_query_terms(query_terms)
    kwargs = dict(index=index, return_type=list)
    with readers.IndexReader(indexed_file, **kwargs) as q:
        yield ["index_value", "count"]
        for row in q.value_count(query_terms):
            yield row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def query_data(indexed_file, query_terms, index=None, verbose=False):
    """Query an ``indexed_file`` using the ``indexes`` and ``query_terms`` for
    actual data rows.

    Parameters
    ----------
    indexed_file : `str`
        The path to the file that is being queried.
    query_terms : `list` of `str`
        The query terms passed by the user. These will be processed into
        queries appropriate for the reader object.
    index : `str`, optional, default: `NoneType`
        The index file to use, if not supplied then it will be detected.
    verbose : `bool`, optional, default: `False`
        Not used at present.

    Yields
    ------
    row : `list`
        Each row is a data row. The first row yielded will be the header.
    """
    query_terms = _process_query_terms(query_terms)
    kwargs = dict(index=index, return_type=list)
    with readers.IndexReader(indexed_file, **kwargs) as q:
        yield q.header
        for row in q.query(query_terms):
            yield row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _process_query_terms(query_terms):
    """Process query terms from strings into tuples used to query the reader
    objects.

    Parameters
    ----------
    query_terms : `list` of `str`
        User defined query terms supplied on the command line.

    Returns
    -------
    queries : `list` of `tuples`
        One or more things to query. Each tuple is a query operator at 0
        and the query value at 1. Valid query operators are: ``=``, ``==``,
        ``=~`` (regex), ``>=``, ``<=``, ``>``, ``<``, ``LIKE``, ``BETWEEN``
        . The regexp operator can have an additional flag at position 2 in
        the tuple indicating any regex flags, such as ``i`` for case
        insensitive. Please note the regex is not valid for SQLite indexes
        and is mapped to a LIKE operator. Additionally, a ``WHERE``
        operator can be given. In this case, the query value at one is
        expected to be a formatted where clause for an SQL query, relevant
        for the index database type.
    """
    process_queries = []
    for val in query_terms:
        val = val.strip()
        opmatch = OPERATOR_REGEX.match(val)
        flags = None

        try:
            val = val[opmatch.span()[1]:].strip()
            op = opmatch.group(0)
            flags = opmatch.group("FLAGS")
            op = re.sub(r'^{0}'.format(flags), '', op)
        except AttributeError:
            op = "=="

        if op.lower() == 'between':
            val, flags = [i.strip() for i in val.split(",")]

        try:
            process_queries.append((op, val, flags.lower()))
        except AttributeError:
            process_queries.append((op, val))

    return process_queries


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _cast_query_terms(query_terms, dtype):
    cast_func = FileColumn.Dtype.get_python_cast(dtype)

    new_terms = []
    for i in query_terms:
        if i[0].lower() != "where":
            print(i[0])
            print(i[1:])
            val = [cast_func(j) for j in i[1:]]
            new_terms.append((i[0], *val))
    return new_terms
